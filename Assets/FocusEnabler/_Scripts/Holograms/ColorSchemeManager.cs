using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorSchemeManager : ToolboxSingleton<ColorSchemeManager>
{
    [Header("Default Enabler")]
    // Background
    public Color DisabledVisible = new Color(100, 100, 100, 1);
    public Color Interactive = new Color(166, 166, 166, 1);
    public Color Focused = Color.white;
    public Color Activated = new Color(100, 100, 100, 1);
    public Color DisabledActivated = new Color(100, 100, 100, 1);

    // Text or Icon
    [Header("Texts or Icons on Enablers")]
    public Color DisabledVisibleText = new Color(64, 64, 64, 1);
    public Color InteractiveText = new Color(64, 64, 64, 1);
    public Color FocusedText = new Color(116, 100, 100, 1);
    public Color ActivatedText = Color.black;
    public Color DisabledActivatedText = new Color(166, 166, 166, 1);

    [Header("Texts or Icons without Background")]
    // Text or Icon without Backgound
    public Color InteractiveWithoutBackground = new Color(166, 166, 166, 1);
    public Color FocusedWithoutBackground = Color.white;
    public Color DisabledWithoutBackground = new Color(100, 100, 100, 1);

    [Header("Progressbar")]
    // Progressbar
    public Color Progressbar = new Color(166, 166, 166, 1);
    public Color ProgressbarRed = new Color(255, 0, 0, 1);

    [Header("Holograms")]
    public Color Hologram = new Color(166, 166, 166, 1);
}
