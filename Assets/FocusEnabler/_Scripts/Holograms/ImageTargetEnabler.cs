using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Viscopic.Holograms
{
    public class ImageTargetEnabler : FocusEnabler
    {
        public float minDistance = 1.5f;

        private bool inPosition = false;
        private Transform mainCamera;
        private Text text;

        private Coroutine UpdateRoutine;

        public override void Init()
        {
            base.Init();

            mainCamera = Camera.main.transform;

            text = this.GetComponentInChildren<Text>(true);
            text.text = "";
        }

        public override void Show()
        {
            base.Show();

            inPosition = false;
            SetDistanceVisualsActive(false);

            if (UpdateRoutine != null)
                StopCoroutine(UpdateRoutine);
            UpdateRoutine = StartCoroutine(UpdateDistanceBasedVisuals());
        }

        public override void Hide(bool immediate = false, bool delay = false)
        {
            if (UpdateRoutine != null)
                StopCoroutine(UpdateRoutine);
            if (text)
                SetDistanceVisualsActive(false);
            base.Hide(immediate, delay);
        }

        IEnumerator UpdateDistanceBasedVisuals()
        {
            // Wait until fading is done
            yield return new WaitUntil(() => !isFadingIn);

            // Canceled by Hiding the Enabler
            while (true)
            {
                CalculateDistance();
                yield return null;
            }
        }
        void SetDistanceVisualsActive(bool active)
        {
            centerImage.enabled = !active;
            text.gameObject.SetActive(active);
        }

        private void CalculateDistance()
        {
            float distance = Vector3.Distance(this.transform.position, mainCamera.transform.position);

            // If angle and distance criteria is met + if its not currently fading in
            if (distance < minDistance) // angle < maxAngle && 
            {
                text.text = "";

                SetDistanceVisualsActive(false);

                if (!isFadingIn)
                {
                    if (!inPosition)
                    {
                        SetInteractive();

                        // Set to true so that the clip is only played once
                        inPosition = true;

                        AudioManager.Instance.PlayClip(AudioManager.DefaultClips.Click);
                    }
                }
            }
            else
            {
                float rounded = Mathf.Round((distance - minDistance) * 10f) / 10f;
                text.text = string.Concat(rounded, "m");

                SetDistanceVisualsActive(true);
                inPosition = false;
                SetDisabledVisible();
            }
        }

    }
}