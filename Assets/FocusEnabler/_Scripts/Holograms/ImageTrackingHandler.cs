using Viscopic;
using HoloToolkit.Unity.InputModule;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using Viscopic.Holograms;

namespace Viscopic.Tracking
{
    /// <summary>
    /// Shows the marker visualization and handles the FocusEnabler.
    /// Place this script on the ImageTargetBehaviour and attach the ImageTargetEnabler as a child
    /// </summary>
    public class ImageTrackingHandler : MonoBehaviour, ITrackableEventHandler
    {
        public string markerID;

        [Space]
        public FocusEnabler enabler;

        private ImageTargetBehaviour imageTarget;
        //private TrackingController controller;

        // Use this for initialization
        void Start()
        {
            Init();
        }

        private void Init()
        {
            imageTarget = GetComponent<ImageTargetBehaviour>();
            if (imageTarget)
                imageTarget.RegisterTrackableEventHandler(this);

            //controller = TrackingController.Instance;

            if (!enabler)
                enabler = GetComponentInChildren<FocusEnabler>();
            if (enabler)
            {
                //enabler.onFocused.AddListener(() => controller.ImageSet(this));
            }
        }

        #region interface members
        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            //controller.TargetDetected(this);

            if (enabler)
            {
                enabler.SetInteractive();
            }
        }

        private void OnTrackingLost()
        {
            if (enabler)
            {
                enabler.Hide(true);
            }
        }

        void OnDestroy()
        {
            if (imageTarget)
                imageTarget.UnregisterTrackableEventHandler(this);
        }
        #endregion
    }
}