﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Holograms
{
    public class RepeatableFocusEnabler : FocusEnabler
    {
        [SerializeField]
        public bool triggerSwitchInstantiation = false;
        public GameObject imageTarget;
        private int max = 60;
        private int counter = 0;
        private bool allowInstantiate = true;

        DBSwitchTrackableEventHandler DBS;
        // Use this for initialization
        void Start()
        {
            //DBS = GameObject.Find("ImageTarget_Switch").GetComponent<DBSwitchTrackableEventHandler>();
            DBS = GetComponentInParent<DBSwitchTrackableEventHandler>();
            if (triggerSwitchInstantiation)
            {
                SwitchTracker.Instance.SetAsSwitchFocusEnabler(this);
            }
            
        }

        void Update()
        {
            if (!allowInstantiate)
            {
                counter++;
                if (counter == max)
                {
                    allowInstantiate = true;
                    counter = 0;
                }
            }
            
            
        }

        public override void SetActivated()
        {
            //TODO: For some bizarre reason this thing gets called 4 times every time, workaround for now please fix.
            if (allowInstantiate && triggerSwitchInstantiation)
            {
                allowInstantiate = false;
                DBS.instantiateSwitch(this.transform);
                Debug.Log("[RepeatableFocusEnabler] " + this.transform.parent.gameObject.name);
                //Debug.Log(Environment.StackTrace);
            }

            if (this.isActiveAndEnabled)
            {
                StartCoroutine(SetInteractiveAfterActivation());
            }

            
            
        }

        private IEnumerator SetInteractiveAfterActivation()
        {

            
            
            base.SetActivated();

            yield return new WaitForSeconds(fadeActiveTime);
            //yield return new WaitForSeconds(7.0f);
            
            

            base.SetInteractive();
        }

        private void OnEnable()
        {
            base.SetInteractive();
        }
    }
}