﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ActiveMeasurementTracker : Singleton<ActiveMeasurementTracker> {
	
    private TextMeshProFade textFade;
    private TextMeshProUGUI text;
    private RawImageFade rawImageFade;
    private Coroutine reminder;
    private float reminderTime = 2.0f;
    private float Distance = 1.0f;
    private Interpolator interpolator;

    private void Start()
    {
        this.text = GetComponentInChildren<TextMeshProUGUI>();
        this.textFade = GetComponentInChildren<TextMeshProFade>();
        this.rawImageFade = GetComponentInChildren<RawImageFade>();
        interpolator = GetComponent<Interpolator>();
        interpolator.PositionPerSecond = 2f;
        this.textFade.MakeInvisible();
        this.rawImageFade.MakeInvisible();
    }

    public void Reminder(float interval)
    {
        this.reminder = StartCoroutine(StartReminder(interval));        
    }

    private IEnumerator StartReminder(float interval)
    {
        Debug.Log("[ActiveMeasurementTracker] Reminder coming in " + interval.ToString() + " seconds.");
        yield return new WaitForSecondsRealtime(interval);
        DVMeasurementPoint mp = SwitchTracker.Instance.GetFirstUnscannedMP();
        if(mp == null)
        {
            yield break;
        }
        SwitchTracker.Instance.ActivateScanFitbox();
        String fortsetzung = mp.currentWorkflowState.measurement.fortsetzung;
        String _name = mp.currentWorkflowState.measurement.name;
        Debug.Log("[ActiveMeasurementTracker] Showing reminder.");
        this.text.text = "Nächste Messung: " + _name + " (" +  fortsetzung + ")"; 
        this.textFade.FadeIn(this.reminderTime / 3);
        this.rawImageFade.FadeIn(this.reminderTime / 3);
        this.rawImageFade.Pulse(0.1f, 1.5f);
        this.textFade.Pulse(0.1f, 1.5f);
    }

    public void StopReminder()
    {
        StopAllCoroutines();
        this.rawImageFade.StopPulse();
        this.textFade.StopPulse();
        this.textFade.MakeInvisible();
        this.rawImageFade.MakeInvisible();
    }

    void LateUpdate()
    {
        Transform cameraTransform = Camera.main.transform;
        interpolator.SetTargetPosition(cameraTransform.position + (cameraTransform.forward * Distance));
        interpolator.SetTargetRotation(Quaternion.LookRotation(cameraTransform.forward, cameraTransform.up));
    }
}
