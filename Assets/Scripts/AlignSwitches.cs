﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;

public class AlignSwitches : Singleton<AlignSwitches> {

    [SerializeField]
    public GameObject canonicalSwitch = null;
    public Vector3 canonicalSwitchPosition;
    public Quaternion canonicalSwitchRotation;

    List<Transform> ties = new List<Transform>();

    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 20;

    public void SetCanonicalSwitch(GameObject can)
    {
        this.canonicalSwitch = can;
        this.canonicalSwitchPosition = can.transform.position;
        this.canonicalSwitchRotation = can.transform.rotation;
    }

    public void AlignHeight()
    {
        if(SwitchTracker.Instance.canonicalSwitch!= null)
        {
            foreach(string name in MarkerCollection.Instance.markers.Keys)
            {
                Transform t = MarkerCollection.Instance.markers[name].transform;
                Vector3 newPos = new Vector3(t.position.x, SwitchTracker.Instance.canonicalSwitch.transform.position.y, t.position.z);
            }
        }
    }

}
