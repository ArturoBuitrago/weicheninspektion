﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManagerWIDV : AnimationManagerDV {

    protected override void ButtonPressed(ButtonTypes buttonType, bool selected, GameObject concernedObject)
    {
        GameObject[] slates = GameObject.FindGameObjectsWithTag("TextSlate");
        switch (buttonType)
        {
            
            case ButtonTypes.Next:
                //Debug.Log("Next");
                
                foreach (GameObject slate in slates)
                {
                    slate.GetComponent<TextSlateDV>().HideCalculation();
                }

                if (SwitchTracker.Instance.currentDVMeasurementPoint.NextWorkflowStateExists())
                {
                    StartCoroutine(SwitchTracker.Instance.currentDVMeasurementPoint.NextWorkflowState());
                }
                else{
                    SwitchTracker.Instance.currentDVMeasurementPoint.WaitForNextMeasurement();
                    StartCoroutine(WaitForNextMarker(selected, concernedObject));
                }                         
                break;

            case ButtonTypes.Previous:

                foreach (GameObject slate in slates)
                {
                    slate.GetComponent<TextSlateDV>().HideCalculation();
                }
                
                GoBackInActiveWorkflow();
                break;

            case ButtonTypes.Exit:
                TextSlatesManagerDV textSlatesManagerDV = (TextSlatesManagerDV)TextSlatesManager.Instance;
                textSlatesManagerDV.HideAllTextSlates();
                break;

            case ButtonTypes.WorkflowList:
                break;

            case ButtonTypes.TapToPlaceTextSlate:
                if (!SwitchTracker.Instance.movingTextSlate)
                {
                    SwitchTracker.Instance.movingTextSlate = true;
                    WIDVTextSlateUtility.Instance.ActivateBillboard(false);
                }
                else
                {
                    WIDVTextSlateUtility.Instance.UpdateTextSlateVector();
                    SwitchTracker.Instance.movingTextSlate = false;
                    WIDVTextSlateUtility.Instance.ActivateBillboard(true);
                }
                base.ButtonPressed(buttonType, selected, concernedObject);
                break;

            case ButtonTypes.PlayOnce:
                Debug.Log("Switch!");
                foreach(GameObject slate in slates)
                {
                    slate.GetComponent<TextSlateDV>().ToggleCalculation();
                }
                break;                

            default:
                base.ButtonPressed(buttonType, selected, concernedObject);
                break;

        }
    }

    private IEnumerator WaitForNextMarker(bool selected, GameObject concernedObject)
    {
        if (selected)
        {
            StartCoroutine(WIDVTextSlateUtility.Instance.FeedbackMPDone());
            yield return new WaitForSecondsRealtime(1.5f);
            TextSlatesManagerDV textSlatesManagerDV = (TextSlatesManagerDV)TextSlatesManager.Instance;
            textSlatesManagerDV.HideAllTextSlates();
            MarkerCollection.Instance.ShowAllAvailableMarkers();
            if (SwitchTracker.Instance.currentDVMeasurementPoint.GetSwitch() != null)
            {
                SwitchTracker.Instance.currentDVMeasurementPoint.GetSwitch().visualFeedback.MarkAsCompleted();
            }
            MarkerCollection.Instance.FindNextYellowMarker();

            if (SwitchTracker.Instance.CheckIfAllInstantiated() && SwitchTracker.Instance.currentOrigin == SwitchTracker.Instance.originMeasurements[0])
            {
                FitBoxInstructions.Instance.ShowMaterial(5);
            }
            else if (SwitchTracker.Instance.CheckIfAllInstantiated() && SwitchTracker.Instance.currentOrigin == SwitchTracker.Instance.originMeasurements[SwitchTracker.Instance.originMeasurements.Count - 1])
            {
                FitBoxInstructions.Instance.ShowMaterial(6);
            }

            SwitchTracker.Instance.EnableSwitchFocusEnabler();
            MarkerCollection.Instance.FindNextYellowMarker();
        }        
    }

    public override void GoBackInActiveWorkflow()
    {
        DVMeasurementPoint mp = SwitchTracker.Instance.currentDVMeasurementPoint;
        mp.PreviousWorkflowState();        
    }
}
