﻿using DVXML;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalculationWindow: MonoBehaviour{
    [SerializeField]
    //public Text titleText;
    public TextSlateDV textSlate;
    public Text titleText;
    public TextMeshProUGUI calculationFormula;
    public TextMeshProUGUI SR100Grenzwert;
    public TextMeshProUGUI SRLimGrenzwert;
    public TextMeshProUGUI Grenzwert;
    int[] intArray = { 256, 512, 16384 };

    public void UpdateFields()
    {
        WorkflowStateDV workflowState = SwitchTracker.Instance.currentDVMeasurementPoint.currentWorkflowState;
        Measurement meas = workflowState.measurement;
        titleText.text = workflowState.name + " | " + meas.descriptionId;
        textSlate.SetTextMeshProText(calculationFormula, meas.calculation.formulaId + ": " + meas.calculation.name);
        textSlate.SetTextMeshProText(SR100Grenzwert, meas.calculation.sr100);
        textSlate.SetTextMeshProText(SRLimGrenzwert, meas.calculation.srlim);
        textSlate.SetTextMeshProText(Grenzwert, "Grenzwerte: " + meas.calculation.grenzwert);
    }
}
