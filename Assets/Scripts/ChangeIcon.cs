﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeIcon : MonoBehaviour {
    [SerializeField]
    Texture _Question, _Exclamation, _Check;
    [SerializeField]
    RawImage rawImage;
    // Use this for initialization
    void Start () {
        rawImage = GetComponent<RawImage>();
	}

    public void ChangeIconTo(string iconName)
    {
        if (iconName == "?")
        {
            rawImage.texture = _Question;
        } else if (iconName == "!")
        {
            rawImage.texture = _Exclamation;
        }
        else
        {
            rawImage.texture = _Check;
        }
    }
}
