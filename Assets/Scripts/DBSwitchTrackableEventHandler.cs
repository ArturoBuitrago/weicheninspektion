/*==============================================================================
Copyright (c) 2017 PTC Inc. All Rights Reserved.

Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using HoloToolkit.Unity.InputModule;
using UnityEngine;
using Vuforia;
using Viscopic.Holograms;
using System;
using UnityEngine.UI;

/// <summary>
///     A custom handler that implements the ITrackableEventHandler interface.
/// </summary>
public class DBSwitchTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{

    public GameObject SwitchGroup;
    private AlignSwitches alignSwitches;

    private bool instantiatedOnce = false;
    public bool trackingEnabled = false;

    private Vector3 hitPosition;
    private float distance;
    private float minDistanceForNextMarker = 0.5f;
    private GazeManager gazeManager;
    private GameObject instantiatedSwitch;

    private Quaternion rotation = Quaternion.Euler(0,0,0);
    private int counter = 0;

    private string detectedName;

    public bool mirrored = false;

    [SerializeField]
    public PulsateImage pulse;
    public bool secondaryTrackable = false;

    private SwitchTracker switchTracker;

    public GameObject Enabler;

    public UnityEngine.UI.Image icon;

    [SerializeField]
    public bool originMarker = false;
    private string measurementImageName = "DB-Image5";
    private bool currentlyTracking = false;

    #region PRIVATE_MEMBER_VARIABLES

    protected TrackableBehaviour mTrackableBehaviour;

    #endregion // PRIVATE_MEMBER_VARIABLES

    #region UNTIY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {
        gazeManager = GameObject.Find("InputManager").GetComponent<GazeManager>();
        alignSwitches = GameObject.Find("Switches").GetComponent<AlignSwitches>();
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        switchTracker = GameObject.Find("SwitchManager").GetComponent<SwitchTracker>();
    }

    internal void ResetOrigin(Transform ts)
    {
        OrientationManager.Instance.MarkerMirrored(ts);
        //Get origin measurement here.
        string color = MarkerCollection.Instance.markers[SwitchTracker.Instance.GetMeasurementPointFromWorkflowStateName(SwitchTracker.Instance.currentOrigin)._name].GetComponentInChildren<VisualFeedback>().currentColor;
        DVMeasurementPoint measurement = SwitchTracker.Instance.GetMeasurementPointFromWorkflowStateName(SwitchTracker.Instance.currentOrigin);
        instantiatedSwitch = Instantiate(this.SwitchGroup, ts.position, rotation, GameObject.Find("Switches").transform);
        instantiatedSwitch.GetComponent<Switch>().measurement = measurement;
        
        instantiatedSwitch.GetComponentInChildren<MarkerName>().UpdateName(measurement.currentWorkflowState.name);
        measurement.alreadyInstantiated = true;
        instantiatedSwitch.SetActive(true);
        if (color == "green")
        {
            instantiatedSwitch.GetComponentInChildren<VisualFeedback>().MarkAsCompleted();
        }
        else if (color == "yellow")
        {
            instantiatedSwitch.GetComponentInChildren<VisualFeedback>().MarkAsActive();
        }
        MarkerCollection.Instance.AddMarker(measurement._name, instantiatedSwitch);
        measurement.SetSwitch(instantiatedSwitch.GetComponent<Switch>());
        instantiatedOnce = true;
        trackingEnabled = false;

        SwitchTracker.Instance.ResetOrigin(instantiatedSwitch);
        this.pulse.FadeInAndOut();
        //HACK
        TextSlatesManagerDV textSlatesManagerDV = (TextSlatesManagerDV)TextSlatesManager.Instance;
        textSlatesManagerDV.HideAllTextSlates();
        this.gameObject.transform.position += new Vector3(100, 100, 100);
        //GameObject.Find("Enabler").GetComponent<RepeatableFocusEnabler>().SetActivated();
        //SwitchTracker.Instance.DisableSwitchFocusEnabler();
    }

    internal void InstantiateOrigin(Transform ts)
    {
        SwitchTracker.Instance.ActivateScanFitbox(false);
        FitBoxInstructions.Instance.StopReminder();
        OrientationManager.Instance.MarkerMirrored(ts);

        DVMeasurementPoint measurement = SwitchTracker.Instance.GetMeasurementByDistance(ts.transform.position);

        Debug.Log("[DBS] Instantiate origin!");
        instantiatedSwitch = Instantiate(this.SwitchGroup, ts.position, rotation, GameObject.Find("Switches").transform);
        instantiatedSwitch.GetComponent<Switch>().measurement = measurement;
        instantiatedSwitch.GetComponentInChildren<MarkerName>().UpdateName(measurement.currentWorkflowState.name);
        measurement.alreadyInstantiated = true;
        SwitchTracker.Instance.ChangeCurrentDVMeasurementPoint(measurement);
        SwitchTracker.Instance.currentWorkflowState = measurement.currentWorkflowState;
        instantiatedSwitch.SetActive(true);
        MarkerCollection.Instance.AddMarker(measurement._name, instantiatedSwitch);
        Debug.Log("[DBS] Origin measurement name: " + measurement._name);
        measurement.SetSwitch(instantiatedSwitch.GetComponent<Switch>());
        instantiatedOnce = true;
        trackingEnabled = false;
        this.pulse.FadeInAndOut();
        TextSlatesManager.Instance.HideAllTextSlates();
        SwitchTracker.Instance.ActivateScanFitbox();
        ActiveMeasurementTracker.Instance.Reminder(0);
        this.gameObject.transform.position += new Vector3(100, 100, 100);
        MarkerCollection.Instance.FindNextYellowMarker();
    }


    #endregion // UNTIY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED)
        {
            this.detectedName = mTrackableBehaviour.TrackableName;
        }

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            this.currentlyTracking = true;
            //This records in Switch Tracker what kind of marker was just detected. It can either be the measurement marker or one of many origin markers.
            //SwitchTracker.Instance.lastMarkerDetected = mTrackableBehaviour.TrackableName;
            OriginMarkerManager.Instance.lastMarkerDetected = mTrackableBehaviour.TrackableName;
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            this.currentlyTracking = false;
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
            OnTrackingLost();
        }
        else
        {
            this.currentlyTracking = false;
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PRIVATE_METHODS



    protected virtual void OnTrackingFound()
    {
        if (this.icon != null)
        {
            this.icon.enabled = true;
        }
        //this.Enabler.gameObject.SetActive(false);
        this.Enabler.GetComponent<FocusEnabler>().SetInteractive();

    }

    public void instantiateSwitch(Transform tr)
    {
        //Mark the last detected marker as actually scanned.
        bool instantiate = OriginMarkerManager.Instance.MarkAsScanned(this, tr);
        //Hacky, should be handled more cleanly.
        if (!instantiate) { Debug.Log("We were instructed NOT to instantiate. Return."); return; }

        FitBoxInstructions.Instance.StopReminder();

        OrientationManager.Instance.MarkerMirrored(tr);

        DVMeasurementPoint measurement = SwitchTracker.Instance.GetMeasurementByDistance(tr.transform.position);

        if(measurement == null)
        {
            return;
        }

        if (OriginMarkerManager.Instance.lastMarkerDetected != OriginMarkerManager.measurementMarker)
        {
            Debug.Log("[DBS] Origin!");
        }
        else
        {
            Debug.Log("[DBS] Measurement!");
        }
        

        Debug.Log("[DBS] Instantiate measurement!");
        OrientationManager.Instance.SetMeasurementDirectionVector(tr);
        instantiatedSwitch = Instantiate(this.SwitchGroup, tr.position, rotation, GameObject.Find("Switches").transform);
        if (measurement.alreadyInstantiated)
        {
            //instantiatedSwitch.GetComponent<Switch>().visualFeedback.MarkAsCompleted();
        }
        instantiatedSwitch.GetComponent<Switch>().measurement = measurement;
        instantiatedSwitch.GetComponentInChildren<MarkerName>().UpdateName(measurement.currentWorkflowState.name);
        measurement.alreadyInstantiated = true;
        SwitchTracker.Instance.ChangeCurrentDVMeasurementPoint(measurement);
        SwitchTracker.Instance.currentWorkflowState = measurement.currentWorkflowState;           
        instantiatedSwitch.SetActive(true);
        MarkerCollection.Instance.AddMarker(measurement._name, instantiatedSwitch);
        measurement.SetSwitch(instantiatedSwitch.GetComponent<Switch>());
        instantiatedOnce = true;
        trackingEnabled = false;
        instantiatedSwitch.GetComponent<Switch>().visualFeedback.MarkAsActive();
        SwitchTracker.Instance.SetArrowTarget(null);
        SwitchTracker.Instance.measurementsInstantiated[measurement._name] = true;
        
        WIDVTextSlateUtility.MoveTextSlate(measurement.GetTextSlateOffset());
        if (measurement.workflowStates[0].measurement.fortsetzung == "MP150")
        {
            WIDVTextSlateUtility.MoveTextSlate(-OrientationManager.Instance.currentMeasurementDirection);
        }
        WIDVTextSlateUtility.Instance.CallAttention();


        //If marker that was detected is an origin marker that has already been scanned.
        if (OriginMarkerManager.Instance.lastMarkerDetected != OriginMarkerManager.measurementMarker && measurement.alreadyInstantiated)
        {
            SwitchTracker.Instance.ResetOrigin(instantiatedSwitch);
        }

        //Why do we do this?
        this.pulse.FadeInAndOut();
        //GameObject.Find("Enabler").GetComponent<RepeatableFocusEnabler>().SetActivated();
        this.gameObject.transform.position += new Vector3(100,100,100);
        SwitchTracker.Instance.DisableSwitchFocusEnabler();        
    }

    private void ShowSwitchGroup()
    {
        Debug.Log("[AlignSwitches] ShowSwitchGroup");
        if (!this.SwitchGroup.activeSelf)
        {
            this.SwitchGroup.SetActive(true);
        }
        this.SwitchGroup.transform.position = this.transform.position;
        Quaternion newRot = new Quaternion();
        newRot = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0);
        //Debug.Log(this.transform.rotation);
        //Debug.Log(newRot);
        this.SwitchGroup.transform.rotation = newRot;
        //this.enabled = false;
        
    }

    private void HideSwitchGroup()
    {
        this.SwitchGroup.SetActive(false);
    }

    protected virtual void OnTrackingLost()
    {
        if (this.icon != null)
        {
            this.icon.enabled = false;
        }
        //this.Enabler.gameObject.SetActive(false);
        this.Enabler.GetComponent<FocusEnabler>().Hide();
    }

    public void MarkerClicked()
    {
        Debug.Log("CLICK");
        Destroy(this.instantiatedSwitch);
        this.instantiatedSwitch = null;
        this.instantiatedOnce = false;
        //HideSwitchGroup();

    }

    public void ToggleActive()
    {
        this.gameObject.SetActive(!gameObject.activeInHierarchy);
    }

    #endregion // PRIVATE_METHODS
}
