﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using DVXML;

public class DVMeasurementPoint{

    //UnityScene
    public string _name;
    public float canonicalDistanceToOrigin;
    public DVMeasurementPoint nextMeasurement;
    public DVMeasurementPoint previousMeasurement;
    public Vector3 currentPosition;
    public Vector3 origin;

    public string originMeasurement;
    public int unitsToOrigin;

    private float distanceToOrigin;
    private float admissibleRadius = 0.8f;
    public bool alreadyInstantiated = false;

    private Switch _switch;

    private MeasurementPoint measurementPoint;
    private List<Measurement> measurements = new List<Measurement>();
    public List<WorkflowStateDV> workflowStates = new List<WorkflowStateDV>();

    public WorkflowStateDV currentWorkflowState;
    private int currentWorkflowStateIndex;

    public GameObject PositionMarker;
    private bool doubleTie;


    //XML DVMeasurementPoint data
    private DVXML.Measurement XMLMeasurement;

    public WorkflowDV workflowDV;

    public DVMeasurementPoint(string _name, Vector3 currentPosition)
    {
        this._name = _name;
        this.currentPosition = currentPosition;
    }

    /*public DVMeasurementPoint(string _name, DVMeasurementPoint originalMeasurement)
    {
        this._name = _name;
        if(currentPosition == default(Vector3)){}
        else
        {
            CalculateDistanceToOrigin(originalMeasurement);
        }
        
    }*/

    public DVMeasurementPoint(string _name)
    {
        this._name = _name;
    }

    public DVMeasurementPoint(WorkflowStateDV workflowState)
    {
        this.measurementPoint = workflowState.CorrespondingMeasurementPoint;
        this.doubleTie = workflowState.CorrespondingMeasurementPoint.doubleTie;
        this.originMeasurement = workflowState.CorrespondingMeasurementPoint.origin;
        this.unitsToOrigin = Int32.Parse(workflowState.CorrespondingMeasurementPoint.unitsToOrigin);
        this._name = this.measurementPoint.name;
        AddWorkflow(workflowState);
        this.currentWorkflowStateIndex = 0;
        this.currentWorkflowState = workflowStates[this.currentWorkflowStateIndex];

        CalculateDistanceToOrigin();
        
    }

    internal void SetAsCurrentMeasurement()
    {
        this.workflowDV.SetWorkflowState(this.currentWorkflowState);
        SwitchTracker.Instance.currentMeasurement = this.measurements[0];        
    }

    public void CalculateDistanceToOrigin()
    {
        this.canonicalDistanceToOrigin = this.unitsToOrigin * SwitchTracker.Instance.stepDistance;
        if (this.doubleTie)
        {
            this.canonicalDistanceToOrigin -= SwitchTracker.Instance.doubleTieOffset;
        }
        //distanceToOrigin = Vector3.Distance(SwitchTracker.Instance.initialPosition, this.currentPosition);
    }

    public void AddWorkflow(WorkflowStateDV workflow)
    {
        this.workflowStates.Add(workflow);
        this.measurements.Add(workflow.measurement);
    }

    public bool NextWorkflowStateExists()
    {
        if (this.workflowStates.Count > this.currentWorkflowStateIndex + 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public IEnumerator NextWorkflowState()
    {        
        CallTransition();
        yield return new WaitForSecondsRealtime(WIDVTextSlateUtility.Instance.textSlateFeedbackTransitionTime);
        SwitchTracker.Instance.previousWorkflowState = this.currentWorkflowState;
        SwitchTracker.Instance.previousMeasurement = this.measurements[currentWorkflowStateIndex];

        this.currentWorkflowStateIndex++;


        this.currentWorkflowState = this.workflowStates[this.currentWorkflowStateIndex];
        this.workflowDV.SetWorkflowState(this.currentWorkflowState);
        SwitchTracker.Instance.currentWorkflowState = this.currentWorkflowState;
        SwitchTracker.Instance.previousMeasurement = this.measurements[currentWorkflowStateIndex];
        //TODO - What measurement needs to be measured on the left? (Sasha)
        if(this.currentWorkflowState.measurement.fortsetzung == "MP150")
        {
            MP150Transition();
        }
        else
        {
            if (this.currentWorkflowState.measurement.descriptionId == "Hoehenlage")
            {
                WIDVTextSlateUtility.MoveTextSlate(_switch.transform);
            }
        }
        
        /*if (currentWorkflowStateIndex == 2 || currentWorkflowStateIndex == 3)
        {
            WIDVTextSlateUtility.MoveTextSlateMirrored(_switch.transform);
        }*/

    }

    private void MP150Transition()
    {
        if(this.currentWorkflowStateIndex == 1)
        {
            //WIDVTextSlateUtility.MoveTextSlate(OrientationManager.Instance.currentMeasurementDirection);
            WIDVTextSlateUtility.MoveTextSlate(_switch.transform);
            WIDVTextSlateUtility.MoveTextSlate(GetTextSlateOffset());
        }else if (this.currentWorkflowStateIndex == 2)
        {
            WIDVTextSlateUtility.MoveTextSlate(_switch.transform);
            WIDVTextSlateUtility.MoveTextSlate(GetTextSlateOffsetLeft());
        }else if(this.currentWorkflowStateIndex == 3)
        {
            //WIDVTextSlateUtility.MoveTextSlate(-GetTextSlateOffsetLeft());
            WIDVTextSlateUtility.MoveTextSlate(_switch.transform);
        }
    }

    private void CallTransition()
    {
        if(this.currentWorkflowStateIndex == 0)
        {
            WIDVTextSlateUtility.Instance.ShowFeedback("left");
        }else if(this.currentWorkflowStateIndex == 1 && this.currentWorkflowState.measurement.fortsetzung == "MP150")
        {
            WIDVTextSlateUtility.Instance.ShowFeedback("left");
        }
        else if (this.currentWorkflowStateIndex == 2 && this.currentWorkflowState.measurement.fortsetzung == "MP150")
        {
            WIDVTextSlateUtility.Instance.ShowFeedback("right");
        }
        else if (this.currentWorkflowStateIndex == 3 && this.currentWorkflowState.measurement.fortsetzung == "MP150")
        {
            WIDVTextSlateUtility.Instance.ShowFeedback("right");
        }
    }

    public void WaitForNextMeasurement()
    {
        this.currentWorkflowStateIndex = 0;
        this.currentWorkflowState = this.workflowStates[0];
        Debug.Log("[DVMeasurementPoint] No next WorkflowState");
        //MarkerCollection.Instance.ShowAllAvailableMarkers();       
    }

    public void PreviousWorkflowState()
    {
        if(this.currentWorkflowStateIndex == 0)
        {
            throw new Exception("No previous workflowstate in the given measurement point.");
        }
        this.currentWorkflowStateIndex--;
        SwitchTracker.Instance.currentWorkflowState = SwitchTracker.Instance.previousWorkflowState;
        SwitchTracker.Instance.currentMeasurement = SwitchTracker.Instance.previousMeasurement;
        SwitchTracker.Instance.previousMeasurement = this.measurements[currentWorkflowStateIndex];
        this.currentWorkflowState = this.workflowStates[this.currentWorkflowStateIndex];
        this.workflowDV.SetWorkflowState(this.currentWorkflowState);
        if (this.currentWorkflowState.measurement.descriptionId == "Spurweite")
        {
            WIDVTextSlateUtility.MoveTextSlate(GetTextSlateOffset());
        }

    }

    internal bool CheckIfCurrentMeasurement(Vector3 currentPosition)
    {
        float currentDistanceToOrigin = Vector3.Distance(currentPosition, SwitchTracker.Instance.initialPosition);
        
        if (currentDistanceToOrigin > Math.Abs(canonicalDistanceToOrigin) - admissibleRadius && currentDistanceToOrigin < Math.Abs(canonicalDistanceToOrigin) + admissibleRadius)
        {
            //Debug.Log("[DVMeasurementPoint : " + this._name + "] - " + currentDistanceToOrigin.ToString());
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SetSwitch(Switch _switch)
    {
        this._switch = _switch;
    }
    public Switch GetSwitch()
    {
        return this._switch;
    }

    internal bool CheckIfPreviousWorkflowState()
    {
        if(this.currentWorkflowStateIndex == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public Vector3 GetTextSlateOffset()
    {
        //Add provision here for specific measurement in which there is a different starting position
        return Vector3.Cross(Vector3.up, OrientationManager.Instance.currentMeasurementDirection).normalized * 0.6f;
    }

    public Vector3 GetTextSlateOffsetLeft()
    {
        return Vector3.Cross(Vector3.down, OrientationManager.Instance.currentMeasurementDirection).normalized * 0.7f;
    }

    //public Vector3 GetTextSlateOffset

}
