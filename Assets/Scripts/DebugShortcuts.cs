﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using HoloToolkit.Unity;

public class DebugShortcuts : Singleton<DebugShortcuts> {

    VoiceHelperDV vh;
        
	// Use this for initialization
	void Start () {
        this.vh = VoiceHelperDV.Instance.gameObject.GetComponent<VoiceHelperDV>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("page down"))
        {
            PageDown();
        }
        if (Input.GetKeyDown("f"))
        {
            print("f key was pressed");
            SwitchTracker.Instance.currentDVMeasurementPoint = SwitchTracker.Instance.measurementsByOrigin[SwitchTracker.Instance.currentOrigin][MarkerCollection.Instance.firstMarker];
            Debug.Log(SwitchTracker.Instance.currentDVMeasurementPoint._name);
        }
        if (Input.GetKeyDown("h"))
        {
            FitBoxInstructions.Instance.ShowMaterial(1);
        }
        if (Input.GetKeyDown("o"))
        {
            this.vh.Okay();
        }
        if (Input.GetKeyDown("p"))
        {
            //VoiceHelperDV.Instance._Start();
            this.vh._Start();
        }
        if (Input.GetKeyDown("n"))
        {
            TutorialHelper.Instance.RepeatInstruction();
        }
        if (Input.GetKeyDown("m"))
        {
            TutorialHelper.Instance.NextTransition();
        }
    }

    private void PageDown()
    {
        print("pagedown key was pressed");
        //this should set everything to already scanned! not just change origin
        List<string> keylist = new List<string>(SwitchTracker.Instance.measurementsInstantiated.Keys);
        foreach (string name in keylist)
        {
            SwitchTracker.Instance.measurementsInstantiated[name] = true;
        }
        foreach (GameObject Marker in MarkerCollection.Instance.markers.Values)
        {
            Marker.GetComponentInChildren<VisualFeedback>().MarkAsCompleted();
        }
    }
}
