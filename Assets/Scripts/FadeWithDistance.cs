﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FadeWithDistance : MonoBehaviour
{

    [SerializeField]
    public float threshold;
    private float distanceToCamera;

    private Renderer _renderer;
    private Material material;
    private Color color;
    private Text text;

    float newAlpha;
    float currentAlpha;

    // Use this for initialization
    void Start()
    {
        threshold = 6f;
        _renderer = GetComponent<MeshRenderer>();
        material = _renderer.material;
        color = material.color;
        text = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        distanceToCamera = Vector3.Distance(this.gameObject.transform.position, Camera.main.transform.position);
        UpdateTransparencyRenderer(distanceToCamera);
        UpdateTransparencyText(distanceToCamera);
    }

    private void UpdateTransparencyRenderer(float distance)
    {
        //Within threshold, lerp the alpha
        if (distance < threshold)
        {
            //Interpolation value:
            float interpolation = distance / threshold;
            newAlpha = Mathf.Lerp(1, 0, interpolation);
            //Debug.Log(newAlpha);
            material.SetColor("_Color", new Color(color[0], color[1], color[2], newAlpha));
            
            
        }
        else if (distance > threshold)
        {
            //Behind threshold, you should see nothing.
            newAlpha = 0f;
            material.SetColor("_BaseColor", new Color(color[0], color[1], color[2], newAlpha));
        }
    }

    private void UpdateTransparencyText(float distance)
    {
        //Within threshold, lerp the alpha
        if (distance < threshold)
        {
            //Interpolation value:
            float interpolation = distance / threshold;
            newAlpha = Mathf.Lerp(1, 0, interpolation);
            //Debug.Log(newAlpha);
            text.color = new Color(color[0], color[1], color[2], newAlpha);


        }
        else if (distance > threshold)
        {
            //Behind threshold, you should see nothing.
            newAlpha = 0f;
            text.color = new Color(color[0], color[1], color[2], newAlpha);
        }
    }

}
