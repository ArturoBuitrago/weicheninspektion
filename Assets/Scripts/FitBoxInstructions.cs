﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;
using UnityEngine.UI;

public class FitBoxInstructions : Singleton<FitBoxInstructions> {
    //Before first scan
    public Material Slide1;
    //After first scan
    public Material Slide2;
    //Error - measurement not found
    public Material Slide3;
    //Error - code not recognized
    public Material Slide4;
    //End of Main track
    public Material Slide5;
    //End of all measurements.
    public Material Slide6;
    public MeshRenderer renderer;

    private Coroutine reminder;

    private bool up = false;
    private bool down = false;

    private bool hold = false;

    int counter = 0;

    private float defaultInterval = 15.0f;

    float totalTime = 4f;

    // Use this for initialization
    void Start () {
        renderer.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        counter++;
        if(counter == 60*0.5*totalTime && up)
        {
            if (!hold)
            {
                up = false;
                down = true;
            }
            
        }
        if(counter == 60*totalTime && down)
        {
            down = false;
            counter = 0;
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], 0);
            renderer.gameObject.SetActive(false);
        }
        if (up)
        {
            //Fade in
            float currentAlpha = renderer.material.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 1.0f, 0.05f);
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], newAlpha);
        }
        else if (down)
        {
            float currentAlpha = renderer.material.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 0f, 0.05f);
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], newAlpha);

            //fade out
        }


    }

    public void ShowMaterial(int i)
    {
        //SwitchTracker.Instance.ActivateScanFitbox(false);
        switch (i)
        {
            case 1:
                SwitchTracker.Instance.ActivateScanFitbox();
                SwitchTracker.Instance.SetScanFitboxText("QR-Code auf der Schwelle am Weichen-Anfang bitte einscannen.");
                break;
            case 2:
                renderer.material = Slide2;
                Debug.Log("Showing Slide" + i);
                FadeInAndOut();
                Reminder(this.defaultInterval, i);
                break;
            case 3:
                renderer.material = Slide3;
                Debug.Log("Showing Slide" + i);
                FadeInAndOut();
                break;
            case 4:
                renderer.material = Slide4;
                Debug.Log("Showing Slide" + i);
                FadeInAndOut();
                break;
            case 5:
                SwitchTracker.Instance.ActivateScanFitbox();
                SwitchTracker.Instance.SetScanFitboxText("Bitte QR-Code am Anfang des Zweiggleis einscannen.");
                renderer.material = Slide5;
                Debug.Log("Showing Slide" + i);
                FadeInAndOut();
                //Reminder(this.defaultInterval, i);
                break;
            case 6:
                renderer.material = Slide6;
                Debug.Log("Showing Slide" + i);
                FadeInAndOut();
                this.hold = true;
                break;
        }
        
    }

    private void FadeInAndOut()
    {
        renderer.gameObject.SetActive(true);
        up = true;        
        counter = 0;
    }

    private IEnumerator StartReminder(float interval, int slideNo)
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(interval);
            ShowMaterial(slideNo);
        }        
    }

    public void StopReminder()
    {
        if(this.reminder != null)
        {
            StopCoroutine(this.reminder);
           
        }
    }

    public void Reminder(float interval, int slideNo)
    {
        if(this.reminder != null)
        {
            StopCoroutine(this.reminder);
        }        
        this.reminder = StartCoroutine(StartReminder(interval, slideNo));
    }

}
