﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceTextureOnMesh : MonoBehaviour {

    private MeshRenderer renderer;
    public Texture texture;

    // Use this for initialization
    void Start () {
        renderer = GetComponent<MeshRenderer>();
        renderer.material.SetTexture("_Albedo",texture);
	}	
}
