﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFade : MonoBehaviour {

    private Image image;
    bool up = false;
    bool down = false;
    float fadeIn = 1f;
    float fadeOut = 1f;
    private int framerate = 60;
    private float startAlpha;
    int counter = 0;

    // Use this for initialization
    void Start () {
        this.image = GetComponent<Image>();

        if(this.image == null)
        {
            throw new System.Exception("No Image found.");
        }
	}

    private void OnEnable()
    {
        Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.up)
        {
            float currentAlpha = this.image.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(this.startAlpha, 1.0f, (float)this.counter / (this.fadeIn * this.framerate));
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            if (this.counter == this.fadeIn * this.framerate)
            {
                this.up = false;
            }
        }
        if (this.down)
        {
            float currentAlpha = this.image.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(this.startAlpha, 0f, (float)this.counter / (this.fadeIn * this.framerate));
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            if (this.counter == this.fadeOut * this.framerate)
            {
                this.down = false;
            }
        }
        this.counter++;
    }

    public void FadeIn(float i)
    {
        this.startAlpha = this.image.color.a;
        if (this.down) { this.down = false; }
        this.fadeIn = i;
        counter = 0;
        this.up = true;
    }

    public void FadeOut(float i)
    {
        this.startAlpha = this.image.color.a;
        if (this.up) { this.up = false; }
        this.fadeOut = i;
        counter = 0;
        this.down = true;
    }
    public void MakeVisible()
    {
        if (this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.image.color = new Color(image.color[0], image.color[1], image.color[2], 1f);
    }
    public void MakeInvisible()
    {
        if (this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.image.color = new Color(image.color[0], image.color[1], image.color[2], 0f);
    }


}
