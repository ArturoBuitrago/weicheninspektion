﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerCollection : Singleton<MarkerCollection> {

    [SerializeField]
    public Dictionary<string, GameObject> markers = new Dictionary<string, GameObject>();
    public bool markersCreated = false;    
    public GameObject markerPrefab;
    public bool showMarkers;
    public string currentMarker;
    public string firstMarker;
    public GameObject pointerToCurrentMarker;

    void Start()
    {
        pointerToCurrentMarker.SetActive(false);
    }

    public void ShowAllAvailableMarkers()
    {
        if (!showMarkers)
        {
            return;
        }
        if (!markersCreated)
        {
            CreateMarkers();
            markersCreated = true;
        }
        foreach (GameObject marker in this.markers.Values)
        {
            marker.SetActive(true);
        }
    }

    public void AddMarker(string name, GameObject marker)
    {
        if (this.markers.ContainsKey(name))
        {
            RemoveMarker(name);
        }
        this.markers.Add(name, marker);
    }

    public void RemoveMarker(string _name)
    {
        GameObject markerToBeRemoved;
        markers.TryGetValue(_name, out markerToBeRemoved);        
        markers.Remove(_name);
        GameObject.Destroy(markerToBeRemoved);
    }

    public void HideAllAvailableMarkers()
    {
        foreach(GameObject marker in this.markers.Values)
        {
            marker.SetActive(false);
        }
        this.pointerToCurrentMarker.SetActive(false);
    }

    public void ClearMarkers()
    {
        foreach (GameObject marker in this.markers.Values)
        {
            Destroy(marker);
        }
        List<string> names = new List<string>(this.markers.Keys);
        foreach(string name in names)
        {
            this.markers.Remove(name);
        }
        markersCreated = false;
        this.pointerToCurrentMarker.SetActive(false);
    }

    private void CreateMarkers()
    {
        //Debug.Log("[MarkerCollection] Creating markers...");
        string origin = SwitchTracker.Instance.currentOrigin;
        string first_ms = "";
        foreach(DVMeasurementPoint measurementPoint in SwitchTracker.Instance.measurementsByOrigin[origin].Values)
        {
            if(first_ms == "")
            {
                first_ms = measurementPoint._name;
                this.firstMarker = measurementPoint._name;
                this.currentMarker = measurementPoint._name;
            }
            Vector3 position = OrientationManager.Instance.direction * measurementPoint.canonicalDistanceToOrigin;
            Quaternion rotation = Quaternion.Euler(0f, 0f, 0f);
            GameObject marker = Instantiate(markerPrefab, position + SwitchTracker.Instance.initialPosition, rotation, this.transform);
            marker.SetActive(false);
            marker.GetComponentInChildren<MarkerName>().UpdateName(measurementPoint.currentWorkflowState.name);
            this.markers.Add(measurementPoint._name, marker);
        }
    }

    private void HighlightFirstMarker()
    {
        this.markers[this.firstMarker].GetComponentInChildren<VisualFeedback>().MarkAsActive();
        SwitchTracker.Instance.SetArrowTarget(this.markers[this.firstMarker].gameObject);
        SwitchTracker.Instance.currentDVMeasurementPoint = SwitchTracker.Instance.measurementsByOrigin[SwitchTracker.Instance.currentOrigin][this.firstMarker];
        HighlightWithPointer(this.markers[this.firstMarker].gameObject);
    }

    public void ResetMarkers()
    {
        List<string> names = new List<string>(this.markers.Keys);
        foreach(string name in names)
        {
            float canonicalDistanceToOrigin = SwitchTracker.Instance.measurementsByOrigin[SwitchTracker.Instance.currentOrigin][name].canonicalDistanceToOrigin;
            this.markers[name].transform.position = SwitchTracker.Instance.initialPosition + OrientationManager.Instance.direction * canonicalDistanceToOrigin;
        }
        FindNextYellowMarker();
    }

    public void AlignHeight(GameObject canonicalSwitch)
    {
        foreach (Transform child in transform)
        {
            if (canonicalSwitch != null)
            {
                Vector3 canon = SwitchTracker.Instance.initialPosition;
                Vector3 newPos = canon + child.position;
            }
        }
    }

    public void FindNextYellowMarker()
    {
        List<GameObject> markerList = new List<GameObject>(this.markers.Values);
        if (SwitchTracker.Instance.currentOrigin != SwitchTracker.Instance.originMeasurements[0])
        {
            markerList.Reverse();
        }

        foreach(GameObject marker in markerList)
        {
            if(marker.GetComponentInChildren<VisualFeedback>().currentColor != "green")
            {
                SwitchTracker.Instance.SetArrowTarget(marker);
                HighlightWithPointer(marker);
                marker.GetComponentInChildren<VisualFeedback>().MarkAsActive();
                marker.GetComponentInChildren<VisualFeedback>()._base.gameObject.SetActive(true);
                marker.GetComponentInChildren<VisualFeedback>().ring.gameObject.SetActive(false);

                return;
            }
        }
    }

    public void HighlightWithPointer(GameObject marker)
    {
        this.pointerToCurrentMarker.SetActive(true);
        this.pointerToCurrentMarker.transform.position = marker.transform.position + new Vector3(0, 1.7f + 0.207461f, 0);
        marker.GetComponentInChildren<VisualFeedback>().solidColor = true;
    }
}
