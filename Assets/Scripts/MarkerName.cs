﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarkerName : MonoBehaviour {

    [SerializeField]
    public Text name;

    public void UpdateName(string _name)
    {
        name.text = _name;
    }
}
