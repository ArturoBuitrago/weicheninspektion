using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public static class MyAlphaTween
{

    public delegate void SetAlpha(float alpha);

    /// <summary>
    /// Fades an alpha value out, typically from 1 to 0.
    /// </summary>
    /// <param name="func">A material with a color and alpha property</param>
    /// <param name="fadeOutTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The ending alpha value</param>
    /// <param name="maxAlpha">The starting alpha value</param>
    public static IEnumerator FadeOut(Material mat, float fadeOutTime = 1f,
        UnityAction call = null, float minAlpha = 0, float maxAlpha = 1)
    {
        Color originalColor = mat.color;
        Color fadedColor = originalColor;
        fadedColor.a = maxAlpha;

        float time = fadeOutTime;

        while (time >= 0)
        {
            fadedColor.a = MapRange((time / fadeOutTime), minAlpha, maxAlpha);

            mat.SetColor("_Color", fadedColor);

            yield return null;
            time -= Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades an alpha value in, typically from 0 to 1.
    /// </summary>
    /// <param name="func">Name of a function that takes a float as argument</param>
    /// <param name="fadeOutTime">Amount of time it takes to fade to the minAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The ending alpha value</param>
    /// <param name="maxAlpha">The starting alpha value</param>
    public static IEnumerator FadeOut(SetAlpha func, float fadeOutTime = 1f,
        UnityAction call = null, float minAlpha = 0, float maxAlpha = 1)
    {
        float alpha = maxAlpha;
        func(alpha);

        float time = fadeOutTime;

        while (time >= 0)
        {
            alpha = MapRange((time / fadeOutTime), minAlpha, maxAlpha);

            func(alpha);

            yield return null;
            time -= Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades an alpha value in, typically from 0 to 1.
    /// </summary>
    /// <param name="func">A material with a color and alpha property</param>
    /// <param name="fadeInTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The starting alpha value</param>
    /// <param name="maxAlpha">The ending alpha value</param>
    public static IEnumerator FadeIn(Material mat, float fadeInTime = 1.5f,
        UnityAction call = null, float minAlpha = 0, float maxAlpha = 1)
    {
        Color originalColor = mat.color;
        Color fadedColor = originalColor;
        fadedColor.a = minAlpha;

        float time = 0;

        while (time <= fadeInTime)
        {
            fadedColor.a = MapRange((time / fadeInTime), minAlpha, maxAlpha);

            mat.SetColor("_Color", fadedColor);

            yield return null;
            time += Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades an alpha value in, typically from 0 to 1.
    /// </summary>
    /// <param name="func">A material with a color and alpha property</param>
    /// <param name="fadeInTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The starting alpha value</param>
    /// <param name="maxAlpha">The ending alpha value</param>
    public static IEnumerator FadeIn(Image image, float fadeInTime = 1.5f,
        UnityAction call = null, float minAlpha = 0, float maxAlpha = 1)
    {
        Color originalColor = image.color;
        Color fadedColor = originalColor;
        fadedColor.a = minAlpha;

        float time = 0;

        while (time <= fadeInTime)
        {
            fadedColor.a = MapRange((time / fadeInTime), minAlpha, maxAlpha);

            image.color = fadedColor;
            yield return null;
            time += Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades an alpha value in, typically from 0 to 1.
    /// </summary>
    /// <param name="func">Name of a function that takes a float as argument</param>
    /// <param name="fadeInTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The starting alpha value</param>
    /// <param name="maxAlpha">The ending alpha value</param>
    public static IEnumerator FadeIn(SetAlpha func, float fadeInTime = 1.5f,
        UnityAction call = null, float minAlpha = 0, float maxAlpha = 1)
    {
        float alpha = minAlpha;
        func(alpha);

        float time = 0;

        while (time <= fadeInTime)
        {
            alpha = MapRange((time / fadeInTime), minAlpha, maxAlpha);

            func(alpha);

            yield return null;
            time += Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades a material in and out.
    /// </summary>
    /// <param name="mat">A material with a color and alpha property</param>
    /// <param name="fadeInTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="fadeOutTime">Amount of time it takes to fade out the minAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The starting alpha value</param>
    /// <param name="maxAlpha">The max Alpha value</param>
    /// <param name="minAlphaEnd">The ending alpha value</param>
    public static IEnumerator FadeInOut(Material mat, float fadeInTime = 1.5f,
        float fadeOutTime = 1.5f, UnityAction call = null,
        float minAlpha = 0, float maxAlpha = 1, float minAlphaEnd = 0)
    {
        Color originalColor = mat.color;
        Color fadedColor = originalColor;
        fadedColor.a = minAlpha;

        float time = 0;

        while (time <= fadeInTime)
        {
            fadedColor.a = MapRange((time / fadeInTime), minAlpha, maxAlpha);

            mat.SetColor("_Color", fadedColor);

            yield return null;
            time += Time.deltaTime;
        }

        time = fadeOutTime;

        while (time >= 0)
        {
            fadedColor.a = MapRange((time / fadeOutTime), minAlphaEnd, maxAlpha);

            mat.SetColor("_Color", fadedColor);

            yield return null;
            time -= Time.deltaTime;
        }

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Fades an object in and out.
    /// </summary>
    /// <param name="mat">Name of a function that takes a float as argument</param>
    /// <param name="fadeInTime">Amount of time it takes to fade to the maxAlpha value</param>
    /// <param name="fadeOutTime">Amount of time it takes to fade out the minAlpha value</param>
    /// <param name="call">A call that will be executed at the end of the coroutine</param>
    /// <param name="minAlpha">The starting alpha value</param>
    /// <param name="maxAlpha">The max Alpha value</param>
    /// <param name="minAlphaEnd">The ending alpha value</param>
    public static IEnumerator FadeInOut(SetAlpha func, float fadeInTime = 1.5f,
    float fadeOutTime = 1f, UnityAction call = null,
    float minAlpha = 0, float maxAlpha = 1, float minAlphaEnd = 0)
    {
        float alpha = minAlpha;
        float time = 0;

        while (time <= fadeInTime)
        {
            alpha = MapRange((time / fadeInTime), minAlpha, maxAlpha);

            func(alpha);

            yield return null;
            time += Time.deltaTime;
        }

        time = fadeOutTime;

        while (time >= 0)
        {
            alpha = MapRange((time / fadeOutTime), minAlphaEnd, maxAlpha);

            func(alpha);

            yield return null;
            time -= Time.deltaTime;
        }

        func(minAlphaEnd);

        if (call != null)
            call.Invoke();
    }

    /// <summary>
    /// Linear Mapping of a value from Range [minA, maxA] -> [minB, maxB]
    /// </summary>
    private static float MapRange(float value, float minB, float maxB, float minA = 0, float maxA = 1)
    {
        return minB + ((maxB - minB) / (maxA - minA)) * (value - minA);
    }
}
