﻿using System;
using HoloToolkit.Unity;
using UnityEngine;

public class OrientationManager : Singleton<OrientationManager>
{
    public Vector3 direction;
    public Vector3 origin;
    public float firstOrientation;
    public Vector3 currentMeasurementDirection;
    private bool mirrored = false;

    void Start()
    {

    }

    /// <summary>
    /// Calculates the direction vector that other measuring points will be aligned towards. We assume that the origin has already been set. 
    /// </summary>
    /// <param name="secondPoint"></param>
    /// <returns></returns>
    public Vector3 CalculateDirectionVector(Vector3 secondPoint)
    {
        if(this.origin == default(Vector3))
        {
            throw new Exception("No origin set, cannot calculate direction vector.");
        }

        Vector3 preliminaryDirectionVector = this.origin - secondPoint;

        if(LiesInFrontOfOrigin(secondPoint - this.origin))
        {
            preliminaryDirectionVector = Vector3.Normalize(secondPoint - this.origin);
            this.origin = preliminaryDirectionVector;
        }
        else
        {
            preliminaryDirectionVector = Vector3.Normalize(-secondPoint + this.origin);
            this.origin = preliminaryDirectionVector;
        }

        return this.origin;
    }

    //This method is now trivial - it will always return 0,0,1 as direction due to the fact that the initial marker's position is defined as the origin.
    public Vector3 CalculateDirectionVector(Transform initialMarker)
    {
        if (this.origin == default(Vector3))
        {
            throw new Exception("No origin set, cannot calculate direction vector.");
        }

        
        Vector3 preliminaryDirectionVector = initialMarker.position - this.origin;
        if(preliminaryDirectionVector == new Vector3(0, 0, 0))
        {
            preliminaryDirectionVector = new Vector3(0, 0, -1);
        }
        

        if (LiesInFrontOfOrigin(preliminaryDirectionVector))
        {
            preliminaryDirectionVector = Vector3.Normalize(preliminaryDirectionVector);
            this.direction = preliminaryDirectionVector;
        }
        else
        {
            preliminaryDirectionVector = Vector3.Normalize(-preliminaryDirectionVector);
            this.direction = preliminaryDirectionVector;
        }

        this.direction = Quaternion.Euler(0, this.firstOrientation, 0) * new Vector3(0,0,1);
        SwitchTracker.textSlateMoveVector = this.direction * 0.5f;

        Debug.Log("[OrientationManager] Setting initial direction : " + this.direction.ToString());
        return this.direction;
    }

    public void SetMeasurementDirectionVector(Transform tf)
    {
        Vector3 cameraDirection = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0) * new Vector3(0, 0, 0.2f);
        this.currentMeasurementDirection = Vector3.Project(cameraDirection, this.direction).normalized * 0.2f;
    }

    public bool LiesInFrontOfOrigin(Vector3 positionOfObject)
    {
        Vector3 crossVector = Vector3.Cross(Quaternion.Euler(0, 90, 0) * this.direction, (positionOfObject - this.origin));
        if(crossVector.y > 0)
        {
            //Behind Origin
            return false;
        }
        else
        {
            //In front of Origin
            return true;
        }
    }

    public bool MarkerMirrored(Transform tf)
    {
        if(this.firstOrientation == default(float))
        {
            this.firstOrientation = tf.rotation.eulerAngles.y;
            Debug.Log("[OrientationManager] Initial orientation set.");
            return true;
        }
        return true;

        // TODO - Only call this in the appropriate place.

        if(Math.Abs(tf.rotation.eulerAngles.y - this.firstOrientation) > 90 && Math.Abs(tf.rotation.eulerAngles.y - this.firstOrientation) < 270)
        {
            if (!mirrored)
            {
                MirrorTextSlateVector();
                mirrored = true;
                //Debug.Log("MIRRORED");
            }
            return true;            
        }
        //Debug.Log("NOT MIRRORED");
        if (mirrored)
        {
            //Debug.Log("RESET MIRRORED");
            MirrorTextSlateVector();
            mirrored = false;
        }
        return false;
    }

    public void ResetDirection(Transform tf)
    {
        this.firstOrientation = tf.rotation.eulerAngles.y;
        this.direction = Quaternion.Euler(0, this.firstOrientation, 0) * new Vector3(0, 0, 1);
    }

    private void MirrorTextSlateVector()
    {
        Vector3 textSlateVector = SwitchTracker.textSlateMoveVector;
        textSlateVector.x = -textSlateVector.x;
        textSlateVector.z = -textSlateVector.z;
        SwitchTracker.textSlateMoveVector = textSlateVector;
    }
}
