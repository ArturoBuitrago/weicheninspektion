﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;

//This class should keep track of the logic behind the 2 marker system: origin markers and the measurement markers.
public class OriginMarkerManager : Singleton<OriginMarkerManager> {

    public string lastMarkerScanned;
    public string lastMarkerDetected;

    public string currentOriginMarker = "";
    public static string measurementMarker = "qr-code_2";

    internal bool MarkAsScanned(DBSwitchTrackableEventHandler DBS, Transform ts)
    {
        FitBoxInstructions.Instance.StopReminder();
        //Mark last detected as last scanned.
        this.lastMarkerScanned = this.lastMarkerDetected;
        if(this.lastMarkerScanned == OriginMarkerManager.measurementMarker)
        {
            //Marker is a measurement marker
            if (SwitchTracker.Instance.CheckIfAllInstantiated())
            {
                FitBoxInstructions.Instance.ShowMaterial(1);
                return false;
            }
            else if (this.currentOriginMarker != "")
            {
                //Origin has been scanned and this is a measurement. Proceed normally.
                return true;
            }
            else
            {
                //There has been no origin scanned to account for this measurement. Throw error.
                FitBoxInstructions.Instance.ShowMaterial(1);
                return false;
            }
        }
        else
        {
            //Marker is an origin marker.
            if(this.currentOriginMarker == "")
            {
                //There has been no origin marker scanned. Take this as the origin.
                this.currentOriginMarker = this.lastMarkerScanned;
                Debug.Log("[OriginMarkerManager] Origin Marker has been scanned for this training facility.");
                OrientationManager.Instance.MarkerMirrored(ts);
                DBS.InstantiateOrigin(ts);
                return false;
            }
            else
            {
                //There has already been an origin marker scanned.
                if(this.lastMarkerScanned == this.currentOriginMarker)
                {
                    //The same origin marker was scanned. Reset Origin.
                    if (SwitchTracker.Instance.CheckIfAllInstantiated() || !MarkerCollection.Instance.markersCreated)
                    {
                        //All measurements corresponding to the given origin have already been instantiated or none have been instantiated. We are waiting for a new origin. All is well.
                        Debug.Log("[OriginMarkerManager] Proceeding to load new markers...");
                        OrientationManager.Instance.ResetDirection(ts);
                        DBS.InstantiateOrigin(ts);
                        return false;
                    }
                    else
                    {
                        Debug.Log("[OriginMarkerManager] Resetting origin...");
                        OrientationManager.Instance.ResetDirection(ts);
                        DBS.ResetOrigin(ts);
                        return false;
                    }
                    
                }
                else
                {
                    //This is a new origin marker. 
                    FitBoxInstructions.Instance.ShowMaterial(4);
                    return false;                    
                }
            }
        }
    }

}
