﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Viscopic.Holograms;

public class PulsateImage : MonoBehaviour {
    [SerializeField]
    public Image image;
    public Image fill;
    public bool colorFeedback = true;
    private bool up = false;
    private bool down = false;
    int counter = 0;
    private Color initialColor;
    public bool enablerIconThere = false;
    private GameObject iconToReplace;

    void Start()
    {
        if (this.image)
        {
            initialColor = this.image.color;
        }
        if (enablerIconThere)
        {
            CheckIfEnablerIcon();
        }       
        
    }

    private void CheckIfEnablerIcon()
    {        
        this.iconToReplace = this.gameObject.transform.parent.parent.Find("Enabler/Icon").gameObject;        
    }

    private void ShutDownEnabler()
    {
        this.gameObject.transform.parent.parent.GetComponentInChildren<RepeatableFocusEnabler>().enabled = false;
    }
    private void RestartEnabler()
    {
        this.gameObject.transform.parent.parent.GetComponentInChildren<RepeatableFocusEnabler>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        this.counter++;
        if (this.counter == 60 * 1.5 && this.up)
        {
            up = false;
            down = true;
        }
        if (this.counter == 60 * 3 && this.down)
        {
            down = false;
            counter = 0;
            this.image.color = initialColor;
            this.fill.color = initialColor;
            if (this.enablerIconThere)
            {
                this.iconToReplace.SetActive(true);
            }
        }
        if (this.up)
        {
            //Fade in
            float currentAlpha = image.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 1.0f, 0.05f);
            if (colorFeedback)
            {
                Color transitionGreen;
                transitionGreen = Color.Lerp(image.color, new Color(0, 255, 0, currentAlpha), 0.02f);
                this.image.color = new Color(transitionGreen[0], transitionGreen[1], transitionGreen[2], newAlpha);
                this.fill.color = new Color(transitionGreen[0], transitionGreen[1], transitionGreen[2], newAlpha);
            }
            else
            {
                this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
                this.fill.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            }            
        }
        else if (this.down)
        {
            float currentAlpha = image.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 0f, 0.05f);
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            this.fill.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            //fade out
        }    
                
    }

    public void FadeInAndOut()
    {
        if (this.enablerIconThere)
        {
            this.iconToReplace.SetActive(false);
        }
        Debug.Log("Fading");
        this.up = true;
        this.counter = 0;
    }

}
