﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pylons : MonoBehaviour {

    public Text textNext;
    public Text textNext2;
    public string[] names;

    public void UpdateTexts(SwitchTracker switchTracker, Switch currentSwitch)
    {

        if(names.Length == 0){ 
            names = switchTracker.measurements.Keys.ToArray();
        }
        string currentSwitchName = currentSwitch.measurement._name;
        int index = Array.IndexOf(names, currentSwitchName);

        int i = 1;

        foreach(Transform child in transform)
        {
            if(names.Length > index + i)
            {
                child.gameObject.GetComponentInChildren<Text>().text = switchTracker.measurements[names[index + i]].currentWorkflowState.name;
            }
            else
            {
                child.gameObject.SetActive(false);
            }
            i++;
        }
    }

}
