﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RawImageFade : MonoBehaviour {

    private RawImage image;
    private bool up = false;
    private bool down = false;
    private float fadeIn = 1f;
    private float fadeOut = 1f;
    private int framerate = 60;
    private float startAlpha;
    private int counter = 0;
    private bool pulse = false;
    private float pulseGoal = 1.0f;
    private float pulseDuration = 1.0f;
    private float a = 0.8f;
    private float b = 1.0f;
    
    

    // Use this for initialization
    void Start () {
        this.image = GetComponent<RawImage>();

        if(this.image == null)
        {
            throw new System.Exception("No RawImage found.");
        }
	}

    private void OnEnable()
    {
        Start();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.up)
        {
            float currentAlpha = this.image.color.a;
            float newAlpha = Mathf.Lerp(this.startAlpha, 1.0f, (float)this.counter / (this.fadeIn * this.framerate));
            //Debug.Log(newAlpha);
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            if (this.counter == this.fadeIn * this.framerate)
            {
                this.up = false;
            }
        }
        if (this.down)
        {
            float currentAlpha = this.image.color.a;
            float newAlpha = Mathf.Lerp(this.startAlpha, 0f, (float)this.counter / (this.fadeIn * this.framerate));
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            if (this.counter == this.fadeOut * this.framerate)
            {
                this.down = false;
            }
        }
        if (this.pulse)
        {
            float currentAlpha = this.image.color.a;
            float newAlpha = Mathf.Lerp(a, 
                b, (float)this.counter / (this.pulseDuration * this.framerate));
            this.image.color = new Color(image.color[0], image.color[1], image.color[2], newAlpha);
            if (this.image.color.a == b)
            {
                float buf = a;
                a = b;
                b = buf;
                this.counter = 0;
            }
        }
        this.counter++;
    }

    public void FadeIn(float i)
    {
        this.startAlpha = this.image.color.a;
        if (this.down) { this.down = false; }
        this.fadeIn = i;
        counter = 0;
        this.up = true;
    }

    public void FadeOut(float i)
    {
        this.startAlpha = this.image.color.a;
        if (this.up) { this.up = false; }
        this.fadeOut = i;
        counter = 0;
        this.down = true;
    }

    public void MakeVisible()
    {
        if(this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.image.color = new Color(image.color[0], image.color[1], image.color[2], 1f);
    }
    public void MakeInvisible()
    {
        if (this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.image.color = new Color(image.color[0], image.color[1], image.color[2], 0f);
    }

    public void Pulse(float percentage, float time)
    {
        MakeInvisible();
        this.pulseGoal = percentage;
        this.a = this.pulseGoal;
        this.b = 1.0f;
        this.pulseDuration = time;
        this.pulse = true;
    }

    public void StopPulse()
    {
        this.pulse = false;
    }
}
