﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderFade : MonoBehaviour {

    private bool up = true;
    private bool down = false;

    int counter = 0;
    public MeshRenderer renderer;
    float totalTime = 2f;

    // Use this for initialization
    void Start () {
        renderer = GetComponent<MeshRenderer>();
	}

    // Update is called once per frame
    void Update()
    {
        counter++;
        if (counter == 60 * 0.5 * totalTime && up)
        {
            up = false;
            down = true;
        }
        if (counter == 60 * totalTime && down)
        {
            down = false;
            up = true;
            counter = 0;
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], 0);
        }
        if (up)
        {
            //Fade in
            float currentAlpha = renderer.material.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 1.0f, 0.05f);
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], newAlpha);
        }
        else if (down)
        {
            float currentAlpha = renderer.material.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(currentAlpha, 0f, 0.05f);
            renderer.material.color = new Color(renderer.material.color[0], renderer.material.color[1], renderer.material.color[2], newAlpha);
        }


    }
}
