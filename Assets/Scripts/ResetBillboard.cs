﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;

public class ResetBillboard : MonoBehaviour {

    private bool flip = false;
    private Quaternion a;
    private Quaternion b;
    private int maxNumberOfFrames = 60;
    private int frameCounter = 0;
	
	void Update () {
        flip = !flip;
        if(flip){
            a = this.transform.rotation;
        }
        else
        {
            b = this.transform.rotation;
        }
        if(a!=default(Quaternion) && b != default(Quaternion))
        {
            float angle = Quaternion.Angle(a, b);
            if (angle != 0)
            {
                frameCounter++;
            }
            
            
            
        }
        if (frameCounter > maxNumberOfFrames)
        {
            frameCounter = 0;
            Reset();
        }
	}

    private void Reset()
    {
        GetComponent<Billboard>().enabled = false;
        Debug.Log("Reset Billboard");
        GetComponent<Billboard>().enabled = true;

    }
    //Get transform rotation
    //If it changes more than a certain amount from frame to frame increase likelihood of jitter
    // when above threshold, reset position. 
}
