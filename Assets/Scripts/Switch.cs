﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Switch : MonoBehaviour {
    [SerializeField]
    public GameObject marker;
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 1;
    public DVMeasurementPoint measurement;
    private SwitchTracker switchTracker;
    public VisualFeedback visualFeedback;
    private bool firstTime = true;
    
    void Start () {
        Debug.Log("[Switch] Switch initialized.");
        switchTracker = GameObject.Find("SwitchManager").GetComponent<SwitchTracker>();
        visualFeedback = GetComponentInChildren<VisualFeedback>();
        if(visualFeedback == null)
        {
            throw new System.Exception("[Switch] Visual Feedback not found.");
        }
    }

    public void OnEnable()
    {
        if (firstTime)
        {
            SwitchTracker.Instance.currentSwitch = this;
            WIDVTextSlateUtility.MoveTextSlate(this.transform);
            GetMeasurement();
            this.gameObject.transform.rotation = GameObject.Find("Switches").GetComponent<AlignSwitches>().canonicalSwitchRotation;
            this.firstTime = false;
        }
        
    }

    public void GetMeasurement()
    {
        SwitchTracker.Instance.SetCanonicalSwitch(this);
        this.measurement.SetAsCurrentMeasurement();
    }

}
