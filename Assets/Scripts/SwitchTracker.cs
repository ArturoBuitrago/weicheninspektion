﻿using System;
using System.Linq;
using System.Collections.Generic;
using HoloToolkit.Unity;
using UnityEngine;
using DVXML;
using Viscopic.Holograms;
using Vuforia;
using TMPro;

public class SwitchTracker : Singleton<SwitchTracker> {

    //Previously, step distance was 1.89m, corresponding to 3S (Schwellen) as often specified in the measurement form. After having measured the different distances and recorded the positions at the
    //installations in munich, we switched to using smaller steps of 1S, which comes to 0.63m
    [SerializeField]
    #region public
    public Vector3 initialPosition;
    public Dictionary<string, DVMeasurementPoint> measurements = new Dictionary<string, DVMeasurementPoint>();
    public Switch currentSwitch = null;
    public WorkflowState currentWorkflowState;
    public WorkflowState previousWorkflowState;
    public DVMeasurementPoint currentDVMeasurementPoint;
    public Measurement currentMeasurement;
    public Measurement previousMeasurement;
    public GameObject FloatingUIObject;    
    public GameObject canonicalSwitch;
    public GameObject lastSwitch;
    public static Vector3 textSlateMoveVector = new Vector3(0, 0, 0.5f);
    public WorkflowDV workflowDV;
    public string currentOrigin = null;
    public Dictionary<string, Dictionary<string,DVMeasurementPoint>> measurementsByOrigin = new Dictionary<string, Dictionary<string, DVMeasurementPoint>>();
    //To check whether we are done with the current section
    public Dictionary<string, bool> measurementsInstantiated;
    public string lastMarkerDetected;
    #endregion
    #region private
    internal float stepDistance = 0.63f;
    internal float doubleTieOffset = 0.33f;
    //For Update function
    private int counter = 0;
    private string initialMeasurementName = null;
    private bool initialPositionSet = false;
    private List<RepeatableFocusEnabler> SwitchFocusEnabler = new List<RepeatableFocusEnabler>();
    private HeadsUpDirectionIndicator headsUpDirectionIndicator;
    #endregion
    protected WorkflowState[] workflowStateList;
    public List<string> originMeasurements = new List<string>();
    internal bool movingTextSlate = false;
    private GameObject ScanFitbox;
    private TextMeshProUGUI tmpScanFitbox;

    // Use this for initialization
    void Start () {
        
        //TextSlatesManager.Instance.HideAllTextSlates();
        headsUpDirectionIndicator = GameObject.Find("HeadsUpDirectionIndicator").GetComponent<HeadsUpDirectionIndicator>();        
        //Get WorkflowStateList
        this.workflowStateList = workflowDV.WorkflowStates;
        CreateMeasurementPoints();
        ScanFitbox = GameObject.Find("ScanFitbox").gameObject;
        this.tmpScanFitbox = ScanFitbox.GetComponentInChildren<TextMeshProUGUI>();
        ActivateScanFitbox(false);
    }

    private void Update()
    {
        counter++;
        if (counter == 120)
        {
            counter = 0;
            AlignSwitches.Instance.AlignHeight(); //This will align the height of the UI measurement markers every 2 seconds.
        }
    }

    public void ActivateScanFitbox(bool active = true)
    {
        SetScanFitboxText("");
        
        this.ScanFitbox.SetActive(active);
    }

    public void SetScanFitboxText(String str)        
    {
        if (str == "")
        {
            this.tmpScanFitbox.enabled = false;
        }
        else
        {
            this.tmpScanFitbox.enabled = true;
            tmpScanFitbox.SetText(str);
            Debug.Log("ScanFitbox has new text: " + str);
        }        
    }

    /// <summary>
    /// This method is called when an origin is scanned, it starts the workflow and saves currentPosition to be referenced accordingly later. 
    /// Essentially sets the origin measurement as the origin.
    /// </summary>
    /// <param name="currentPosition">Position of the measurement to be marked as origin. This will be the position of the focus enabler when it fills up.</param>
    public void InitializeMeasurement(Vector3 currentPosition)
    { 
        this.workflowDV.StartWorkflow(this.workflowStateList[0].name, this.workflowStateList.Last().name ,WorkflowMode.Train);
        this.initialPosition = currentPosition;
        OrientationManager.Instance.origin = this.initialPosition;
        this.measurementsByOrigin[this.currentOrigin][this.initialMeasurementName].currentPosition = currentPosition;
        this.measurementsByOrigin[this.currentOrigin][this.initialMeasurementName].canonicalDistanceToOrigin = 0f;

        foreach (DVMeasurementPoint measurement in this.measurementsByOrigin[this.currentOrigin].Values)
        {
            measurement.origin = this.initialPosition;
        }
        initialPositionSet = true;
    }

    /// <summary>
    /// Gets the measurement closest to currentPosition, according to their distances to origin. If all measurements for the given origin have already been measured, 
    /// this will return the new origin after calling the appropriate method to instantiate the corresponding markers.
    /// Ambiguities get resolved by cross-checking currentPosition and whether it lies in front or behind origin. 
    /// </summary>
    /// <param name="currentPosition"></param>
    /// <returns>Closest DVMeasurementPoint to currentPosition.</returns>
    public DVMeasurementPoint GetMeasurementByDistance(Vector3 currentPosition)
    {
        DVMeasurementPoint returnMeasurement = null;
        List<DVMeasurementPoint>returnMeasurements = new List<DVMeasurementPoint>();
        //Check that not everything has been instantiated.
        if (CheckIfAllInstantiated())
        {
            ChangeOrigin();
            return this.measurementsByOrigin[this.currentOrigin][this.initialMeasurementName];
        }


        //Insert provision for first time this method gets called.
        if (!this.initialPositionSet)
        {
            return GetMeasurementPointFromWorkflowStateName(this.currentOrigin);
        }

        foreach (DVMeasurementPoint measurement in this.measurementsByOrigin[this.currentOrigin].Values)
        {
            if (measurement.CheckIfCurrentMeasurement(currentPosition))
            {
                //If the measured point lies in front of the origin and the measurement that we have found has negative units to origin, continue to the next one (we have selected the wrong one)
                if ((OrientationManager.Instance.LiesInFrontOfOrigin(currentPosition) && measurement.unitsToOrigin < 0) || !OrientationManager.Instance.LiesInFrontOfOrigin(currentPosition) && measurement.unitsToOrigin > 0)
                {
                    continue;
                }
                returnMeasurements.Add(measurement);
            }
        }

        returnMeasurement = GetClosest(currentPosition, returnMeasurements);
        
        
        if (returnMeasurement == null)
        {
            FitBoxInstructions.Instance.ShowMaterial(3);
            Debug.Log("DVMeasurementPoint not found (distance " + Vector3.Distance(currentPosition, new Vector3(0, 0, 0)).ToString() + " )!");
        }else
        {
            Debug.Log("Closest measurement point found: " + returnMeasurement._name);
        }

        return returnMeasurement;
    }

    /// <summary>
    /// Auxiliary method to GetMeasurementByDistance, returns the closest measurement point to currentPosition given the provided list of DVMeasurementPoints.
    /// </summary>
    /// <param name="currentPosition">Position against which to compare the measurements.</param>
    /// <param name="returnMeasurements">List of measurements to be compared.</param>
    /// <returns>Closest DVMeasurementPoint to currentPosition. </returns>
    private DVMeasurementPoint GetClosest(Vector3 currentPosition, List<DVMeasurementPoint> returnMeasurements)
    {
        float distance = Vector3.Distance(this.initialPosition, currentPosition);
        float min_diff = float.MaxValue;
        DVMeasurementPoint returnMeasurement = null;
        foreach(DVMeasurementPoint mp in returnMeasurements)
        {
            float dif = Math.Abs(mp.canonicalDistanceToOrigin - distance);
            if (dif <= min_diff)
            {
                min_diff = dif;
                returnMeasurement = mp;
            }
        }
        return returnMeasurement;
    }

    /// <summary>
    /// Called when the origin Switch gets instantiated, sets flags and saves a couple of important variables (e.g. direction vector).
    /// The origin switch is always the first one to be instantiated.
    /// </summary>
    /// <param name="_switch">Switch script of the canonical (origin) switch.</param>
    public void SetCanonicalSwitch(Switch _switch)
    {
        if (!this.initialPositionSet)
        {
            InitializeMeasurement(_switch.transform.position);
            _switch.visualFeedback.isFirstSwitch = true;
            GameObject.Find("Switches").GetComponent<AlignSwitches>().SetCanonicalSwitch(_switch.gameObject);
            this.canonicalSwitch = _switch.gameObject;
            this.initialPositionSet = true;
            OrientationManager.Instance.CalculateDirectionVector(_switch.transform);
            MarkerCollection.Instance.ShowAllAvailableMarkers();
        }
    }

    /// <summary>
    /// Sets the heads up direction indicator arrow to the target GameObject.
    /// </summary>
    /// <param name="target">Target GameObject for arrow to follow.</param>
    public void SetArrowTarget(GameObject target)
    {
        this.headsUpDirectionIndicator.SetTarget(target);
    }
    
    /// <summary>
    /// Changes current DVMeasurementPoint to measurementPoint. Removes the corresponding marker from the MarkerCollection, since the switch object already contains 
    /// a floating UI object. If not one of the origin measurement points, also hides the other markers for the duration of the measurement, until the TextSlate has 
    /// been clicked away.
    /// </summary>
    /// <param name="measurementPoint">New current DVmeasurementPoint.</param>
    public void ChangeCurrentDVMeasurementPoint(DVMeasurementPoint measurementPoint)
    {
        this.currentDVMeasurementPoint = measurementPoint;
        MarkerCollection.Instance.RemoveMarker(measurementPoint._name);
        MarkerCollection.Instance.HideAllAvailableMarkers();

        if (!this.originMeasurements.Contains(measurementPoint.currentWorkflowState.name))
        {
            //Hide the markers for every measurement, including origin measurements!
            //MarkerCollection.Instance.HideAllAvailableMarkers();
        }        
    }

    /// <summary>
    /// Finds and returns the DVMeasurementPoint associated with a given WorkflowState. Useful since its the WorkflowStates that carry the names that the hand measurement
    /// form names (i.e. sa4, sez, etc). Throws an exception if no such WFS found.
    /// </summary>
    /// <param name="searchString">Name of the WorkflowState to use as a search term.</param>
    /// <returns>DVMeasurementPoint associated with the found WorkflowState.</returns>
    public DVMeasurementPoint GetMeasurementPointFromWorkflowStateName(String searchString)
    {
        foreach (DVMeasurementPoint ms in measurementsByOrigin[this.currentOrigin].Values)
        {
            foreach (WorkflowStateDV wfsd in ms.workflowStates)
            {
                if (searchString == wfsd.name)
                {
                    return ms;
                }
            }
        }
        throw new Exception("[SwitchTracker] No measurement point found with the specified workflowstate in the given origin.");
    }

    /// <summary>
    /// Finds and returns the DVMeasurementPoint associated with a given WorkflowState. Useful since its the WorkflowStates that carry the names that the hand measurement
    /// form names (i.e. sa4, sez, etc). Throws an exception if no such WFS found.
    /// </summary>
    /// <param name="wfs">WorkflowState to use as a search term.</param>
    /// <returns>DVMeasurementPoint associated with the found WorkflowState.</returns>
    public DVMeasurementPoint GetMeasurementPointFromWorkflowState(WorkflowState wfs)
    {
        foreach(DVMeasurementPoint ms in measurements.Values)
        {
            foreach(WorkflowStateDV wfsd in ms.workflowStates)
            {
                if(wfs.name == wfsd.name)
                {
                    return ms;
                }
            }            
        }
        throw new Exception("[SwitchTracker] No measurement point found with the specified workflowstate.");
    }

    /// <summary>
    /// Adds the specified RepeatableFocusEnabler to the list of focus enablers. They are kept in a list in order to better activate / deactivate them all at the same time.
    /// </summary>
    /// <param name="repeatableFocusEnabler">RepeatableFocusEnabler to be added to the list.</param>
    internal void SetAsSwitchFocusEnabler(RepeatableFocusEnabler repeatableFocusEnabler)
    {
        this.SwitchFocusEnabler.Add(repeatableFocusEnabler);           
    }

    /// <summary>
    /// Enables all FocusEnablers, reactivates Vuforia behavior. Happens when TextSlate has been clicked away.
    /// </summary>
    internal void EnableSwitchFocusEnabler()
    {
        Debug.Log("[SwitchTracker] FocusEnablers now enabled.");
        ActiveMeasurementTracker.Instance.Reminder(0);
        foreach(RepeatableFocusEnabler enabler in this.SwitchFocusEnabler)
        {
            enabler.gameObject.SetActive(true);
        }
        VuforiaBehaviour.Instance.enabled = true;
        
        
    }

    /// <summary>
    /// Disables all FocusEnablers, deactivates Vuforia behavior. Happens when TextSlate has been made visible.
    /// </summary>
    internal void DisableSwitchFocusEnabler()
    {
        Debug.Log("[SwitchTracker] FocusEnablers now disabled.");
        ActiveMeasurementTracker.Instance.StopReminder();
        SwitchTracker.Instance.ActivateScanFitbox(false);
        foreach (RepeatableFocusEnabler enabler in this.SwitchFocusEnabler)
        {
            enabler.gameObject.SetActive(false);
        }
        VuforiaBehaviour.Instance.enabled = false;
        //ScanFitbox.gameObject.SetActive(false);
    }

    /// <summary>
    /// Called at the very start of this class, it creates the appropriate DVMeasurementPoints from existing WorkflowStateDVs.
    /// These are then queried later during the measurement process.
    /// </summary>
    private void CreateMeasurementPoints()
    {
        DVMeasurementPoint previousMP = null;
        //Create appropriate DVMeasurementPoint from existing WorkflowStateDVs

        foreach (WorkflowStateDV workflow in workflowStateList)
        {
            //change the WorkflowStateNavigation because otherwise it causes trouble. This is hacky.
            workflow.workflowStateType = WorkflowState.WorkflowStateNavigation.NextButton;

            string _name = workflow.CorrespondingMeasurementPoint.name;
            //If MeasurementPointDV already exists, add WorkflowDV to it.
            if (this.measurements.ContainsKey(_name))
            {
                this.measurements[_name].AddWorkflow(workflow);
            }
            //Otherwise, create it and initialize it properly.
            else
            {
                //Check if measurement point has a new origin. If so, add it to existing list.
                string origin = workflow.CorrespondingMeasurementPoint.origin;
                if (!this.originMeasurements.Contains(origin))
                {
                    this.originMeasurements.Add(origin);
                    //for the first origin found, set as current origin (we assume measurementPoints are written in sequential order)
                    if(this.currentOrigin == "")
                    {
                        this.currentOrigin = origin;
                        //this.initialMeasurementName = origin;
                    }
                }

                DVMeasurementPoint ms = new DVMeasurementPoint(workflow);
                ms.previousMeasurement = previousMP;
                if (previousMP != null)
                {
                    previousMP.nextMeasurement = ms;
                }
                previousMP = ms;
                ms.workflowDV = this.workflowDV;

                if(initialMeasurementName == null && ms.canonicalDistanceToOrigin == 0f)        
                {
                    this.initialMeasurementName = ms._name;
                }
                
                //Legacy measurements
                measurements.Add(_name, ms);
                
                //if it does not contain key, create new dictionary
                if (!measurementsByOrigin.ContainsKey(ms.originMeasurement))
                {
                    measurementsByOrigin.Add(ms.originMeasurement, new Dictionary<string, DVMeasurementPoint>());
                    measurementsByOrigin[ms.originMeasurement].Add(_name, ms);
                }
                else
                //if it does, just add it
                {
                    measurementsByOrigin[ms.originMeasurement].Add(_name, ms);
                }
            }

        }

        CreateMeasurementsInstantiated(this.currentOrigin);
    }

    /// <summary>
    /// Auxiliary function for CreateMeasurementPoints, creates the dictionary that keeps track of which measurements have already been instantiated. 
    /// This dictionary then gets checked to see when a change of origin measurement can happen - when all measurements associated to a given origin have been instantiated already.
    /// </summary>
    /// <param name="origin">Name of the origin measurement whose corresponding measurements have to be tracked.</param>
    private void CreateMeasurementsInstantiated(string origin)
    {
        if (!this.originMeasurements.Contains(origin))
        {
            throw new Exception("[SwitchTracker] Given origin measurement not found.");
        }

        measurementsInstantiated = new Dictionary<string, bool>();
        foreach(DVMeasurementPoint mp in measurementsByOrigin[origin].Values)
        {
            measurementsInstantiated.Add(mp._name, false);
        }
    }

    /// <summary>
    /// Checks if all measurements corresponding to the active origin measurement have already been instantiated.
    /// </summary>
    /// <returns>True if all appropriate measurements have been instantiated, false otherwise.</returns>
    public bool CheckIfAllInstantiated()
    {
        if(this.measurementsInstantiated == default(Dictionary<string, bool>))
        {
            return false;
        }
        foreach(bool status in this.measurementsInstantiated.Values)
        {
            if (!status)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Checks if none of the measurements corresponding to the active origin measurement have already been instantiated.
    /// </summary>
    /// <returns>True if all appropriate measurements have not been instantiated, false otherwise.</returns>
    public bool CheckIfNoneInstantiated()
    {
        if (this.measurementsInstantiated == default(Dictionary<string, bool>))
        {
            return true;
        }
        foreach (bool status in this.measurementsInstantiated.Values)
        {
            if (status)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Changes the origin to the next origin in the list (or if already at the last element, circles back to the first element).
    /// Resets the instantiated measurement dictionary, clears the visual markers in preparation for the next origin to be scanned. 
    /// </summary>
    public void ChangeOrigin()
    {
        //Change current origin to new one, create measurements instantiated for new origin, get rid of old markers
        int originIndex = this.originMeasurements.IndexOf(this.currentOrigin);
        if (originIndex + 1 >= this.originMeasurements.Count) { originIndex = 0; } else { originIndex++; }
        this.currentOrigin = this.originMeasurements.ElementAt(originIndex);
        Debug.Log("[SwitchTracker] Current origin: " + this.currentOrigin);

        //Create measurements instantiated
        CreateMeasurementsInstantiated(this.currentOrigin);

        //Clear markers
        MarkerCollection.Instance.ClearMarkers();

        //Set new initial measurement name
        foreach(DVMeasurementPoint mp in this.measurementsByOrigin[this.currentOrigin].Values)
        {
            if(mp.currentWorkflowState.name == this.currentOrigin)
            {
                this.initialMeasurementName = mp._name;
            }
        }

        //Setting the conditions for next scan to be the origin

        this.initialPositionSet = false;
        
    }

    /// <summary>
    /// Resets the current origin. Called when markers need to be readjusted or similar circumstances. Resets the position and orientation of the 
    /// origin switch, resets the markers accordingly.
    /// </summary>
    /// <param name="newSwitch">Switch of the newly resetted origin.</param>
    public void ResetOrigin(GameObject newSwitch)
    {
        Debug.Log("[SwitchTracker] Reset Origin");
        SetCanonicalSwitch(newSwitch.GetComponent<Switch>());
        this.initialPosition = newSwitch.transform.position;
        MarkerCollection.Instance.ResetMarkers();
    }

    public DVMeasurementPoint GetFirstUnscannedMP()
    {
        foreach(String id in this.measurementsInstantiated.Keys){
            if(this.measurementsInstantiated[id] == false)
            {
                return this.measurementsByOrigin[this.currentOrigin][id];
            }
        }        
        return null;
    }
}
