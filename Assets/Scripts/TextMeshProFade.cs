﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextMeshProFade : MonoBehaviour {

    private TextMeshProUGUI text;
    private bool up = false;
    private bool down = false;
    private float fadeIn = 1f;
    private float fadeOut = 1f;
    private int framerate = 60;
    private float startAlpha;
    private int counter = 0;
    private bool pulse = false;
    private float pulseGoal = 1.0f;
    private float pulseDuration = 1.0f;
    private float a = 0.8f;
    private float b = 1.0f;

    // Use this for initialization
    void Start () {
        this.text = GetComponent<TextMeshProUGUI>();

        if (this.text == null)
        {
            throw new System.Exception("No TextMeshProUGUI found.");
        }
    }
	
    private void OnEnable()
    {
        Start();
    }

    void Update()
    {
        if (this.up)
        {
            float currentAlpha = this.text.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(this.startAlpha, 1.0f, (float)this.counter / (this.fadeIn * this.framerate));
            //Debug.Log(newAlpha);
            this.text.color = new Color(text.color[0], text.color[1], text.color[2], newAlpha);
            if (this.counter == this.fadeIn * this.framerate)
            {
                this.up = false;
            }
        }
        if (this.down)
        {
            float currentAlpha = this.text.color.a;
            float newAlpha = 0;
            newAlpha = Mathf.Lerp(this.startAlpha, 0f, (float)this.counter / (this.fadeIn * this.framerate));
            this.text.color = new Color(text.color[0], text.color[1], text.color[2], newAlpha);
            if (this.counter == this.fadeOut * this.framerate)
            {
                this.down = false;
            }
        }
        if (this.pulse)
        {
            float currentAlpha = this.text.color.a;
            float newAlpha = Mathf.Lerp(a,
                b, (float)this.counter / (this.pulseDuration * this.framerate));
            this.text.color = new Color(text.color[0], text.color[1], text.color[2], newAlpha);
            if (this.text.color.a == b)
            {
                float buf = a;
                a = b;
                b = buf;
                this.counter = 0;
            }
        }
            //Debug.Log(counter);
        this.counter++;
    }

    public void FadeIn(float i)
    {
        this.startAlpha = this.text.color.a;
        if (this.down) { this.down = false; }
        this.fadeIn = i;
        counter = 0;
        this.up = true;
    }

    public void FadeOut(float i)
    {
        this.startAlpha = this.text.color.a;
        if (this.up) { this.up = false; }
        this.fadeOut = i;
        counter = 0;
        this.down = true;
    }

    public void MakeVisible()
    {
        if (this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.text.color = new Color(text.color[0], text.color[1], text.color[2], 1f);
    }
    public void MakeInvisible()
    {
        if (this.up || this.down)
        {
            this.up = false;
            this.down = false;
        }
        this.text.color = new Color(text.color[0], text.color[1], text.color[2], 0f);
    }

    public void Pulse(float percentage, float time)
    {
        MakeInvisible();
        this.pulseGoal = percentage;
        this.a = this.pulseGoal;
        this.b = 1.0f;
        this.pulseDuration = time;
        this.pulse = true;
    }

    public void StopPulse()
    {
        this.pulse = false;
    }
}
