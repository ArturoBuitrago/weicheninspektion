﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DVXML;
using TMPro;
using System;

public class TextSlateDV : TextSlate {

    public TextMeshProUGUI Fortseztung;
    public TextMeshProUGUI Sollwert;
    public TextMeshProUGUI SR100_minus;
    public TextMeshProUGUI SR100_plus;
    public TextMeshProUGUI SRlim_minus;
    public TextMeshProUGUI SRlim_plus;
    public TextMeshProUGUI Grenzwert_plus;
    public TextMeshProUGUI Grenzwert_minus;
    public TextMeshProUGUI SR100_tolerance;
    public TextMeshProUGUI SRlim_tolerance;

    public GameObject window;
    public GameObject calculationWindow;

    public TextMeshProUGUI CalculationButtonText;
    public RawImage CalculationButtonIcon;
    public Texture CalculationIcon;
    public Texture MeasurementIcon;

    int[] intArray = { 64, 256, 512};

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public virtual void UpdateFields()
    {
        Debug.Log("[TextSlateDV] Fields updated.");
        WorkflowStateDV workflowState = SwitchTracker.Instance.currentDVMeasurementPoint.currentWorkflowState;
        Measurement meas = workflowState.measurement;
        base.titleText.text = workflowState.name + " | " + meas.descriptionId;
        if(meas.fortsetzung == "")
        {
            base.SetTextMeshProText(Fortseztung, "Fortsetzung: " + SwitchTracker.Instance.previousMeasurement.fortsetzung);
        }
        else
        {
            base.SetTextMeshProText(Fortseztung, "Fortsetzung: " + meas.fortsetzung);
        }                
        base.SetTextMeshProText(Sollwert,"Sollwert: "  + meas.sollwert);
        base.SetTextMeshProText(SR100_minus, "- : " + meas.sr100Minus);
        base.SetTextMeshProText(SR100_plus, "+ : " + meas.sr100Plus);
        base.SetTextMeshProText(SRlim_plus, "+ : " + meas.srlimPlus);
        base.SetTextMeshProText(SRlim_minus, "- : " + meas.srlimMinus);
        base.SetTextMeshProText(Grenzwert_minus, "- : " + meas.grenzwertMinus);
        base.SetTextMeshProText(Grenzwert_plus, "+ : " + meas.grenzwertPlus);

        foreach (int integer in intArray)
        {
            base.SetDisplayTypeGameObjectActive((TextSlateDisplayFlags) integer, true);
        }

        base.ShowDisplayType(TextSlateDisplayFlags.Text);

        CheckButtons();
    }

    private void CheckButtons()
    {
        if (!SwitchTracker.Instance.currentDVMeasurementPoint.CheckIfPreviousWorkflowState())
        {
            base.PreviousBasicButton.gameObject.SetActive(false);
        }
        else
        {
            base.PreviousBasicButton.gameObject.SetActive(true);
        }

        base.TapToPlaceButton.gameObject.SetActive(false);
        
    }

    internal void SetTextMeshProText(TextMeshProUGUI field, string v)
    {
        base.SetTextMeshProText(field, v);
    }

    override protected void DisableAllDisplayTypeGameObjects()
    {
        goText.SetActive(false);
        goTextWithImage.SetActive(false);
        goTextWithSymobl.SetActive(false);
        goGraph.SetActive(false);
        goCount.SetActive(false);
        if (goList != null)
        {
            goList.SetActive(false);
        }

        PlayButton.SetActive(false);
        CloseBasicButton.gameObject.SetActive(false);
        ChecklistBasicButton.gameObject.SetActive(false);
        NextBasicButton.gameObject.SetActive(false);
        PreviousBasicButton.gameObject.SetActive(false);

#if !UNITY_ANDROID
        TapToPlaceButton.gameObject.SetActive(false);
#endif


        if (WorkflowListButton != null)
        {
            WorkflowListButton.gameObject.SetActive(false);
        }
    }

    public void ToggleCalculation()
    {
        if (calculationWindow.activeSelf)
        {
            HideCalculation();
        }
        else
        {
            ShowCalculation();
        }
    }    

    public void ShowCalculation()
    {
        if (!calculationWindow.activeSelf)
        {
            calculationWindow.SetActive(true);
            calculationWindow.GetComponent<CalculationWindow>().UpdateFields();
            CalculationButtonIcon.texture = MeasurementIcon;
            base.SetTextMeshProText(CalculationButtonText, "Messung");
        }
        if (window.activeSelf)
        {
            window.SetActive(false);
        }
    }

    public void HideCalculation()
    {
        if (calculationWindow.activeSelf)
        {
            calculationWindow.SetActive(false);
            CalculationButtonIcon.texture = MeasurementIcon;
            CalculationButtonIcon.texture = CalculationIcon;
            base.SetTextMeshProText(CalculationButtonText, "Berechnung");
        }
        if (!window.activeSelf)
        {
            window.SetActive(true);
        }
    }
    
}
