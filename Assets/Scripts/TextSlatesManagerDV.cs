﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using System;

class TextSlatesManagerDV : TextSlatesManager {

    public override void Start()
    {        
        Debug.Log("[TextSlatesManagerDV] TextSlatesManager is dead. Long live TextSlatesManagerDV");
        base.Start();
    }

    override internal void PositionGroup(GameObject group, bool visible, TextSlate textSlate)
    {
        if (!SwitchTracker.Instance)
        {
            return;
        }
        if (SwitchTracker.Instance.currentSwitch != null)
        {
            WIDVTextSlateUtility.MoveTextSlate(SwitchTracker.Instance.currentSwitch.transform);
        }
    }

    public override void UpdateFieldsDV()
    {
        foreach (GameObject obj in textSlateGroups)
        {
            TextSlateDV tslate = obj.GetComponentInChildren<TextSlateDV>();
            if (tslate != null)
            {
                tslate.UpdateFields();
            }
        }
    }

    internal void Test()
    {
        Debug.Log("Test passed");
    }
}
