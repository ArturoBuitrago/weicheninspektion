﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Viscopic.Holograms;
using HoloToolkit.Unity;
using TMPro;

public class TutorialHelper : Singleton<TutorialHelper> {
    public PulsateImage pulse1;
    public PulsateImage pulse2;
    public RepeatableFocusEnabler enabler1; //First marker
    public RepeatableFocusEnabler enabler2; //First button
    public RepeatableFocusEnabler enabler3; //Small markers @ corner
    public RepeatableFocusEnabler enabler4; //All other next buttons
    public RepeatableFocusEnabler enabler5; //Small marker in slide 7
    public RepeatableFocusEnabler enabler6; //Small marker in slide 15
    public RepeatableFocusEnabler enabler7; //Previous buttons enablers
    public RepeatableFocusEnabler enabler8; //Repeat button enablers
    public RepeatableFocusEnabler timeEnabler;
    public List<RepeatableFocusEnabler> enablerList = new List<RepeatableFocusEnabler>();
    public GameObject canvas;
    private int fadeTime = 2;
    private int currentTransition = 0;
    private TutorialPanelFade tutPanel;
    private float floatTimeStep = 0.2f;
    private float startFloatTime = 2.2f;
    private Color blue = new Color(44, 124, 184, 255);
    private WaitInterruptable wait;
    // Use this for initialization
    void Start() {
        this.gameObject.transform.position = Camera.main.transform.position + Vector3.ProjectOnPlane(Camera.main.transform.forward, new Vector3(0, 1, 0)) * 2.0f;
        this.gameObject.transform.eulerAngles = new Vector3(0, Camera.main.transform.eulerAngles.y, 0);
        VoiceHelperDV.Instance.SetTutorialHelper(this);
        tutPanel = canvas.GetComponent<TutorialPanelFade>();
        this.tutPanel.ChangeTexture(1);
        tutPanel.FadeIn(this.fadeTime);
        Vuforia.VuforiaBehaviour.Instance.enabled = false;
        this.enablerList.Add(enabler1);
        this.enablerList.Add(enabler2);
        this.enablerList.Add(enabler3);
        this.enablerList.Add(enabler4);
        this.enablerList.Add(enabler5);
        this.enablerList.Add(enabler6);
        this.enablerList.Add(enabler7);
        this.enablerList.Add(enabler8);
        this.wait = GetComponent<WaitInterruptable>();
        SetFocusTime(this.startFloatTime);
        ShowTimer(17.0f);
        StartCoroutine(StartCountdown());
        //SwitchTracker.Instance.SetArrowTarget(this.gameObject);
    }

    private IEnumerator StartCountdown()
    {
        yield return this.wait.WaitCoroutine(17.0f);
        StartCoroutine(Transition(1));
    }
    private void StopCountdown()
    {
        this.timeEnabler.ForceStopFocus();
        this.wait.Stop();
    }

    internal void _Start()
    {
        EndTutorial();

    }

    internal void Okay()
    {

        StartCoroutine(Transition(1));
    }

    // Update is called once per frame
    void Update() {

    }

    public void TriggerTransition(int i)
    {
        StartCoroutine(Transition(i));
    }

    public void PreviousInstruction()
    {
        HideButtons();
        if (this.currentTransition < 3)
        {
            this.currentTransition = 1;

        }
        else if (this.currentTransition > 2 && this.currentTransition < 5)
        {
            this.currentTransition = 1;
        }
        else if (this.currentTransition > 4 && this.currentTransition < 8)
        {
            this.currentTransition = 1;
        }
        else if (this.currentTransition > 7 && this.currentTransition < 10)
        {
            this.currentTransition = 5;
        }
        else if (this.currentTransition > 9 && this.currentTransition < 12)
        {
            this.currentTransition = 8;
        }
        else if(this.currentTransition == 12)
        {
            this.currentTransition = 8;
        }
        StartCoroutine(Transition(this.currentTransition));
    }

    public void RepeatInstruction()
    {
        HideButtons();
        if (this.currentTransition < 3)
        {
            this.currentTransition = 1;

        }
        else if (this.currentTransition > 2 && this.currentTransition < 5)
        {
            this.currentTransition = 4;
        }
        else if (this.currentTransition > 4 && this.currentTransition < 8)
        {
            this.currentTransition = 5;
        }
        else if (this.currentTransition > 7 && this.currentTransition < 10)
        {
            this.currentTransition = 8;
        }
        else if (this.currentTransition > 9 && this.currentTransition < 12)
        {
            this.currentTransition = 10;
        }
        else if(this.currentTransition == 12)
        {
            this.currentTransition = 1;
        }
        StartCoroutine(Transition(this.currentTransition));
    }

    public IEnumerator Transition()
    {
        StartCoroutine(Transition(this.currentTransition));
        yield return null;        
    }

    public IEnumerator Transition(int i)
    {
        Debug.Log("Transition: " + i);
        this.currentTransition = i;
        switch (i)
        {
            case 1:
                StopCountdown();
                ShowTimer(6.0f);
                this.tutPanel.ChangeTexture(2);
                canvas.transform.Find("CodeImage").gameObject.SetActive(true);
                //yield return this.wait.WaitCoroutine(2.0f);
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(Transition(2));
                DeactivateOkay();
                break;
            case 2:
                //ShowTimer(7.5f);
                StartCoroutine(this.tutPanel.TransitionTexture(3,1f));
                //this.tutPanel.ChangeTexture(3);
                yield return this.wait.WaitCoroutine(1.5f);
                canvas.transform.Find("CodeImage").GetComponent<ImageFade>().FadeIn(2.5f);
                yield return this.wait.WaitCoroutine(2.5f);
                ReactivateEnabler(enabler1);
                //enabler1.gameObject.SetActive(true);                
                break;
            case 3:
                //ShowTimer(4.0f);
                canvas.transform.Find("CodeImage").gameObject.SetActive(false);
                enabler1.gameObject.SetActive(false);
                pulse1.FadeInAndOut();
                this.tutPanel.FadeOut(2);
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(Transition(4));
                enabler1.gameObject.SetActive(false);
                break;
            case 4:
                ShowTimer(4.5f);
                this.tutPanel.FadeIn(1);
                this.tutPanel.ChangeTexture(4);
                yield return this.wait.WaitCoroutine(2.5f);
                StartCoroutine(this.tutPanel.TransitionTexture(5,2.5f));
                yield return this.wait.WaitCoroutine(2.0f);
                canvas.transform.Find("CalculationButton").gameObject.SetActive(true);
                enabler2.gameObject.SetActive(true);
                break;
            case 5:
                ShowTimer(5.0f);
                canvas.transform.Find("CalculationButton").gameObject.SetActive(false);
                enabler2.gameObject.SetActive(false);
                StartCoroutine(this.tutPanel.TransitionTexture(6,1.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(7, 3.0f));
                yield return this.wait.WaitCoroutine(2.5f);
                enabler5.gameObject.SetActive(true);
                //enabler5.OnFocusEnter();
                //yield return this.wait.WaitCoroutine(6.0f);
                break;
            case 6:
                ShowTimer(6.0f);
                //yield return this.wait.WaitCoroutine(1.0f);
                enabler5.gameObject.SetActive(false);
                StartCoroutine(this.tutPanel.TransitionTexture(8, 1.0f));
                yield return this.wait.WaitCoroutine(4.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(9, 3.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                enabler3.gameObject.SetActive(true);
                //enabler3.OnFocusEnter();
                break;
            case 7:
                //enabler3.OnFocusExit();
                //enabler3.enabled = false;
                ShowTimer(1.0f + 2.0f + 1.0f + 2.0f+ 1.0f + 0.25f);
                enabler3.gameObject.SetActive(false);
                StartCoroutine(this.tutPanel.TransitionTexture(10, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(11, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(12, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(13, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                ShowButtons();
                break;
            case 8:
                HideButtons();
                ShowTimer(4.0f);                
                StartCoroutine(this.tutPanel.TransitionTexture(14, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(15, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                enabler6.gameObject.SetActive(true);
                //enabler6.OnFocusEnter();
                break;
            case 9:
                ShowTimer(1.0f + 4.0f + 3.0f + 4.0f);
                //yield return this.wait.WaitCoroutine(1.0f);
                enabler6.gameObject.SetActive(false);
                StartCoroutine(this.tutPanel.TransitionTexture(16, 1.0f));
                yield return this.wait.WaitCoroutine(4.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(17, 1.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(18, 1.0f));
                yield return this.wait.WaitCoroutine(1.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(19, 1.0f));
                yield return this.wait.WaitCoroutine(1.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(20, 1.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                ShowButtons();
                break;
            case 10:
                HideButtons();
                ShowTimer(1.0f + 3.0f +1.0f + 3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(21, 1.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(22, 1.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(23, 1.0f));
                yield return this.wait.WaitCoroutine(1.5f);
                ReactivateEnabler(this.enabler3);
                //enabler3.SetInteractive();
                //enabler3.OnFocusEnter();
                break;
            case 11:
                enabler3.gameObject.SetActive(false);
                ShowTimer(2.5f + 1.0f + 4.0f);
                yield return this.wait.WaitCoroutine(1.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(24, 2.0f));
                yield return this.wait.WaitCoroutine(3.0f);
                StartCoroutine(this.tutPanel.TransitionTexture(25, 1.0f));
                yield return this.wait.WaitCoroutine(4.0f);
                ShowButtons();
                break;
            case 12:
                HideButtons();
                StartCoroutine(this.tutPanel.TransitionTexture(26, 1.0f));
                yield return this.wait.WaitCoroutine(2.0f);
                ShowButtons();
                ChangeTextLastButtons();
                break;
            case 13:
                HideButtons();
                DestroyEnabler(2);
                EndTutorial();
                break;
            default:

                //yield return new WaitForSeconds(0);
                break;
        }
    }

    private void ShowButtons()
    {
        ReactivateEnabler(this.enabler4);
        ReactivateEnabler(this.enabler7);
        ReactivateEnabler(this.enabler8);
        canvas.transform.Find("NextButton").gameObject.SetActive(true);
        canvas.transform.Find("PreviousButton").gameObject.SetActive(true);
        canvas.transform.Find("RepeatButton").gameObject.SetActive(true);
    }
    private void HideButtons()
    {
        this.enabler4.gameObject.SetActive(false);
        this.enabler7.gameObject.SetActive(false);
        this.enabler8.gameObject.SetActive(false);
        canvas.transform.Find("NextButton").gameObject.SetActive(false);
        canvas.transform.Find("PreviousButton").gameObject.SetActive(false);
        canvas.transform.Find("RepeatButton").gameObject.SetActive(false);
    }

    private void ShowButtonsNoRepeat()
    {
        ReactivateEnabler(this.enabler4);
        ReactivateEnabler(this.enabler7);
        //ReactivateEnabler(this.enabler8);
        canvas.transform.Find("NextButton").gameObject.SetActive(true);
        canvas.transform.Find("PreviousButton").gameObject.SetActive(true);
        //canvas.transform.Find("RepeatButton").gameObject.SetActive(true);
    }

    public void DecreaseFocusTime()
    {
        foreach(RepeatableFocusEnabler rfe in this.enablerList)
        {
            float time = rfe.focusTime;
            if(time <= 1.0f)
            {
                rfe.focusTime = 1.0f;
            }
            else
            {
                rfe.focusTime -= this.floatTimeStep;
            }
        }
    }

    public void IncreaseFocusTime()
    {
        foreach (RepeatableFocusEnabler rfe in this.enablerList)
        {
            float time = rfe.focusTime;
            if (time >= 3.0f)
            {
                rfe.focusTime = 3.0f;
            }
            else
            {
                rfe.focusTime += this.floatTimeStep;
            }
        }
    }

    public void SetFocusTime(float newTime)
    {
        foreach (RepeatableFocusEnabler rfe in this.enablerList)
        {
           
            rfe.focusTime = newTime;
            
        }
    }

    public void NextTransition()
    {
        StopAllCoroutines();
        this.timeEnabler.ForceStopFocus();
        this.wait.Stop();
        this.currentTransition++;
        StartCoroutine(Transition(this.currentTransition));
        DecreaseFocusTime();
    }

    public void PreviousTransition()
    {
        StopAllCoroutines();
        this.timeEnabler.ForceStopFocus();
        this.wait.Stop();
        this.currentTransition--;
        StartCoroutine(Transition(this.currentTransition));
        IncreaseFocusTime();
    }

    private IEnumerator WaitThenTransition(float seconds, int transitionNo)
    {
        yield return new WaitForSeconds(seconds);
        //Transition(transitionNo);
    }

    private IEnumerator JustWait(float seconds)
    {
        yield return new WaitForSeconds(seconds);
    }

    public void DestroySlate(int i)
    {
        switch (i)
        {
            case 1:
                Destroy(canvas);
                break;
            case 2:
                Destroy(canvas);
                break;
            default:
                Destroy(canvas);
                break;
        }
    }

    public void DestroyEnabler(int i)
    {
        switch (i)
        {
            case 1:                
                Destroy(enabler1);
                break;
            case 2:
                Destroy(enabler2);
                break;
        }            

    }
    public void EndTutorial()
    {
        Destroy(this.gameObject);
        Vuforia.VuforiaBehaviour.Instance.enabled = true;
        SwitchTracker.Instance.SetArrowTarget(null);
        FitBoxInstructions.Instance.ShowMaterial(1);
    }
    public void DeactivateVoice()
    {
        VoiceHelperDV.Instance.ClearTutorialHelper();
    }
    public void DeactivateOkay()
    {
        VoiceHelperDV.Instance.DeactivateOkay();
    }

    private void ShowTimer(float time)
    {
        this.timeEnabler.transform.Find("Fill").GetComponent<Image>().color = this.blue;
        this.timeEnabler.allowInteraction = true;
        this.timeEnabler.buttonState = FocusEnabler.ButtonState.Focused;
        this.timeEnabler.focusTime = time;
        this.timeEnabler.ForceFocus(this.blue);
    }

    private void ReactivateEnabler(RepeatableFocusEnabler rfe)
    {
        rfe.allowInteraction = true;
        rfe.buttonState = FocusEnabler.ButtonState.Interactive;
        rfe.GetComponent<Collider>().enabled = true;
        rfe.gameObject.SetActive(true);
    }

    private void ChangeTextLastButtons()
    {
        this.transform.Find("Canvas/NextButton/UIButtonSquare/CalculationCanvas/Text").GetComponent<TextMeshProUGUI>().text = "Starten";
    }
}
