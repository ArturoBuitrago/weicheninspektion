﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanelFade : MonoBehaviour {

    [SerializeField]
    Renderer rend;
    TextMeshProUGUI text;
    int fadeIn = 1;
    int fadeOut = 1;
    bool up = false;
    bool down = false;
    int counter=0;
    private RawImage rawImage;
    private RawImage screen;
    private RawImageFade rawImageFade;
    private RawImageFade screenFade;
    private int framerate = 60;
    public Texture _1;
    public Texture _2;
    public Texture _3;
    public Texture _4;
    public Texture _5;
    public Texture _6;
    public Texture _7;
    public Texture _8;
    public Texture _9;
    public Texture _10;
    public Texture _11;
    public Texture _12;
    public Texture _13;
    public Texture _14;
    public Texture _15;
    public Texture _16;
    public Texture _17;
    public Texture _18;
    public Texture _19;
    public Texture _20;
    public Texture _21;
    public Texture _22;
    public Texture _23;
    public Texture _24;
    public Texture _25;
    public Texture _26;

    private float startAlpha;
    float transitionTime = 2.0f;


    // Use this for initialization
    void Start () {
        //rend = transform.Find("Window/Background").GetComponent<Renderer>();
        //text = transform.Find("Window/Canvas/ViewPort/Text").GetComponent<TextMeshProUGUI>();
        this.rawImage = transform.Find("PanelImage").GetComponent<RawImage>();
        this.screen = transform.Find("TransitionScreen").GetComponent<RawImage>();
        this.rawImageFade = transform.Find("PanelImage").GetComponent<RawImageFade>();
        this.screenFade = transform.Find("TransitionScreen").GetComponent<RawImageFade>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void FadeIn(int i)
    {
        rawImageFade.FadeIn(i);
    }

    public void FadeOut(int i)
    {
        rawImageFade.FadeOut(i);
    }

    public IEnumerator switchOnAndOff(int seconds, bool toSwitch)
    {
        this.counter = 0;
        if (toSwitch) { this.up = true; } else { this.down = true;}
        yield return new WaitForSecondsRealtime(seconds);
        //if (toSwitch) { this.up = false; } else { this.down = false; }
    }

    public IEnumerator switchOnAndOff(int seconds, bool toSwitch, TutorialHelper tut, int slate)
    {
        this.counter = 0;
        if (toSwitch) { this.up = true; } else { this.down = true; }
        yield return new WaitForSecondsRealtime(seconds);
        //if (toSwitch) { this.up = false; } else { this.down = false; }
    }

    public void ChangeTexture(int i)
    {
        switch (i)
        {
            case 1:
                //Transition(i);
                this.rawImage.texture = _1;
                break;
            case 2:
                this.rawImage.texture = _2;
                break;
            case 3: this.rawImage.texture = _3; break;
            case 4:
                this.rawImage.texture = _4;
                break;
            case 5:
                this.rawImage.texture = _5;
                break;
            case 6: this.rawImage.texture = _6; break;
            case 7: this.rawImage.texture = _7; break;
            case 8: this.rawImage.texture = _8; break;
            case 9: this.rawImage.texture = _9; break;
            case 10: this.rawImage.texture = _10; break;
            case 11: this.rawImage.texture = _11; break;
            case 12: this.rawImage.texture = _12; break;
            case 13: this.rawImage.texture = _13; break;
            case 14: this.rawImage.texture = _14; break;
            case 15: this.rawImage.texture = _15; break;
            case 16: this.rawImage.texture = _16; break;
            case 17: this.rawImage.texture = _17; break;
            case 18: this.rawImage.texture = _18; break;
            case 19: this.rawImage.texture = _19; break;
            case 20: this.rawImage.texture = _20; break;
            case 21: this.rawImage.texture = _21; break;
            case 22: this.rawImage.texture = _22; break;
            case 23: this.rawImage.texture = _23; break;
            case 24: this.rawImage.texture = _24; break;
            case 25: this.rawImage.texture = _25; break;
            case 26: this.rawImage.texture = _26; break;
            default:
                break;
        }
    }
    public void ChangeTexture(int i, RawImage raw)
    {
        switch (i)
        {
            case 1:
                //Transition(i);
                raw.texture = _1;
                break;
            case 2:
                raw.texture = _2;
                break;
            case 3: raw.texture = _3; break;
            case 4:
                raw.texture = _4;
                break;
            case 5:
                raw.texture = _5;
                break;
            case 6: raw.texture = _6; break;
            case 7: raw.texture = _7; break;
            case 8: raw.texture = _8; break;
            case 9: raw.texture = _9; break;
            case 10: raw.texture = _10; break;
            case 11: raw.texture = _11; break;
            case 12: raw.texture = _12; break;
            case 13: raw.texture = _13; break;
            case 14: raw.texture = _14; break;
            case 15: raw.texture = _15; break;
            case 16: raw.texture = _16; break;
            case 17: raw.texture = _17; break;
            case 18: raw.texture = _18; break;
            case 19: raw.texture = _19; break;
            case 20: raw.texture = _20; break;
            case 21: raw.texture = _21; break;
            case 22: raw.texture = _22; break;
            case 23: raw.texture = _23; break;
            case 24: raw.texture = _24; break;
            case 25: raw.texture = _25; break;
            case 26: raw.texture = _26; break;
            default:
                break;
        
        }
    }

    public IEnumerator TransitionTexture(int i, float time)
    {
        ChangeTexture(i, this.screen);
        screenFade.FadeIn(time);
        yield return new WaitForSecondsRealtime(time);        
        ChangeTexture(i);
        screenFade.MakeInvisible();
    }

}
