﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualFeedback : MonoBehaviour {

    [SerializeField]
    public bool isFirstSwitch = false;
    public Image _base;
    public Image ring;
    public Text ringText;

    public Sprite RedBase;
    public Sprite RedCircle;
    public Sprite YellowBase;
    public Sprite YellowCircle;
    public Sprite GreenBase;
    public Sprite GreenCircle;

    private float distance;
    private float threshold = 1.0f;
    private float changeArea = 1.0f;
    private float fadeDistance = 7.0f;
    private float invisibleDistance = 10.0f;
    private Camera camera;
    public string currentColor = "";

    float newAlpha;
    float currentAlpha;

    public bool solidColor = false;

    private Switch _switch;

    void Start () {
        
        camera = Camera.main;
        if(this.currentColor == "")
        {
            currentColor = "red";
        }
        this._switch = GetComponentInParent<Switch>();
    }

    void Update()
    {
        distance = Vector3.Distance(camera.gameObject.transform.position, b: this.gameObject.transform.position);
        UpdateTransparencyBase(distance);
        UpdateTransparencyRing(distance);
        UpdateTransparencyText(distance);
    }


    private float GetNewAlpha(float distance)
    {
        //If we are too close
        if (distance < threshold)
        {            
            return 0f;
        }
        //If we are too far away
        else if(distance > invisibleDistance)
        {
            if (this.solidColor) { return 1f; }
            return 0f;
        }
        else if (distance > threshold + changeArea && distance < fadeDistance)
        {
            return 1.0f;
        }
        else if (distance > threshold && distance < threshold + changeArea)
        {
            
            return  (distance - threshold) / (changeArea);
        }
        else if (distance < fadeDistance)
        {
            return 1.0f;
        }
        else
        {
            if (this.solidColor) { return 1f; }
            return (invisibleDistance - distance) / (invisibleDistance - fadeDistance);
        }
    }

    private void UpdateTransparencyRing(float distance)
    {
        float newAlpha = GetNewAlpha(distance);

        ring.color = new Color(ring.color[0], ring.color[1], ring.color[2], newAlpha);      
    }

    private void UpdateTransparencyText(float distance)
    {
        float newAlpha = GetNewAlpha(distance);

        Color textColor = ringText.color;
        ringText.color = new Color(textColor[0], textColor[1], textColor[2], newAlpha);
    }


    private void UpdateTransparencyBase(float distance)
    {
        float newAlpha = GetNewAlpha(distance);


        _base.color = new Color(_base.color[0], _base.color[1], _base.color[2], newAlpha);
    }

    private void ChangeColor(String color)
    {
        switch (color)
        {
            case "red":
                _base.sprite = RedBase;
                ring.color = Color.white;
                ring.sprite = RedCircle;
                _base.color = Color.white;
                break;
            case "green":                
                _base.sprite = GreenBase;
                _base.color = Color.white;
                ring.sprite = GreenCircle;
                ring.color = Color.white;
                break;
            case "yellow":                
                _base.sprite = YellowBase;
                ring.color = Color.white;
                ring.sprite = YellowCircle;
                _base.color = Color.white;
                break;
            default:
                break;
        }
    }

    public void MarkAsCompleted()
    {
        this.currentColor = "green";
        Debug.Log("[VisualFeedback] MeasurementPoint marked as completed (green).");
        _base.gameObject.SetActive(false);
        ring.gameObject.SetActive(true);
        ChangeColor("green");     
        this.solidColor = false;        
    }

    public void MarkAsActive()
    {
        _base.gameObject.SetActive(true);
        ring.gameObject.SetActive(false);
        this.currentColor = "yellow";
        Debug.Log("[VisualFeedback] MeasurementPoint marked as active (yellow).");
        ChangeColor(this.currentColor);
    }

    public void MarkAsUnavailable()
    {
        _base.gameObject.SetActive(false);
        ring.gameObject.SetActive(true);
        this.currentColor = "red";
        Debug.Log("RED");
        ChangeColor(this.currentColor);
    }
}
