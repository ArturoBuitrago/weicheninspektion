﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System;
using UnityEngine.Events;

public class VoiceHelperDV : Singleton<VoiceHelperDV> {

    public TutorialHelper tutorialHelper;
    private SpeechInputHandler SIH;
    private bool okay = true;

    public void SetTutorialHelper(TutorialHelper tut)
    {
        this.tutorialHelper = tut;
        //Debug.Log("Microphone :" + Microphone.devices);
        Debug.Log("TutorialHelper set!");
    }

    public void Okay()
    {
        Debug.Log("OKAY");
        if (tutorialHelper && this.okay)
        {
            tutorialHelper.Okay();
        }
    }

    public void _Start()
    {
        Debug.Log("START");
        if (tutorialHelper)
        {
            tutorialHelper._Start();
        }
    }

    public void ClearTutorialHelper()
    {
        this.tutorialHelper = null;
    }

    internal void DeactivateOkay()
    {
        this.okay = false;
    }
}
