﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VuforiaEnabler : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Camera.main.gameObject.GetComponent<VuforiaBehaviour>().enabled = true;
	}
}
