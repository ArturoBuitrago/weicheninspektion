﻿using HoloToolkit.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WIDVTextSlateUtility : Singleton<WIDVTextSlateUtility> {

    [SerializeField]
    //private GameObject TextSlatePrefab;
    public float textSlateFeedbackTransitionTime = 1.0f;
    public float textSlateFeedbackWaitTime = 1.5f;
    

    private Color niceYellow = new Color(251f/255f, 176f/255f, 64f/255f);
    private Color niceGreen = new Color(141f/255f, 198f / 255f, 63f / 255f);
    private Color niceRed = new Color(237f / 255f, 28f / 255f, 36f / 255f);

    private void OnEnable()
    {
        GameObject.Find("WorkflowWindowTextSlates").GetComponent<Billboard>().enabled = false;
    }

    public void UpdateTextSlateVector()
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {

            SwitchTracker.textSlateMoveVector = textSlate.transform.position - SwitchTracker.Instance.currentSwitch.transform.position;

        }
    }

    public void ActivateBillboard(bool active)
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.GetComponent<Billboard>().enabled = active;
        }
    }

    public static void MoveTextSlate(Transform currentMarkerTransform)
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.position = currentMarkerTransform.position + OrientationManager.Instance.currentMeasurementDirection;
        }
    }

    public static void MoveTextSlateMirrored(Transform currentMarkerTransform)
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.position = currentMarkerTransform.position - OrientationManager.Instance.currentMeasurementDirection;
        }
    }

    public static void MoveTextSlate(WorkflowState wfs)
    {
        Vector3 moveVector = new Vector3(0, 0, 0.5f);
        Vector3 positionWfs = new Vector3(0, 0, SwitchTracker.Instance.GetMeasurementPointFromWorkflowState(wfs).canonicalDistanceToOrigin);

        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            Debug.Log("Hacky move!, relative position: " + (positionWfs - textSlate.transform.position));
            textSlate.transform.position = positionWfs + moveVector;
        }
    }

    internal static void MoveTextSlate(Vector3 movementVector)
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.position = textSlate.transform.position + movementVector;
        }
    }

    public void ShowFeedback(string type){
        switch (type)
        {
            case "left":
                StartCoroutine(NextSlateLeft());
                break;
            case "right":
                StartCoroutine(NextSlateRight());
                break;
            default:
                break;
        }
    }

    public IEnumerator FeedbackMPDone()
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.Find("FeedbackImage").GetComponent<Image>().color = new Color(this.niceGreen.r, this.niceGreen.g, this.niceGreen.b, 0);
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);            
            textSlate.transform.Find("FeedbackImage/FeedbackCheck").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            textSlate.transform.Find("FeedbackImage/TextDone").GetComponent<TextMeshProFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            yield return new WaitForSecondsRealtime(this.textSlateFeedbackWaitTime);
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/FeedbackCheck").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/TextDone").GetComponent<TextMeshProFade>().MakeInvisible();
        }
        WIDVTextSlateUtility.Instance.CallAttention(true);
    }

    public IEnumerator NextSlateLeft()
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.Find("FeedbackImage").GetComponent<Image>().color = new Color(this.niceYellow.r, this.niceYellow.g, this.niceYellow.b, 0);
            //textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            textSlate.transform.Find("FeedbackImage/ArrowLeft").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            textSlate.transform.Find("FeedbackImage/TextNotDone").GetComponent<TextMeshProFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            yield return new WaitForSecondsRealtime(this.textSlateFeedbackWaitTime);
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/ArrowLeft").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/TextNotDone").GetComponent<TextMeshProFade>().MakeInvisible();
        }
        WIDVTextSlateUtility.Instance.CallAttention();
    }

    public IEnumerator NextSlateRight()
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            textSlate.transform.Find("FeedbackImage").GetComponent<Image>().color = new Color(this.niceYellow.r, this.niceYellow.g, this.niceYellow.b, 0);
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            textSlate.transform.Find("FeedbackImage/ArrowRight").GetComponent<ImageFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            textSlate.transform.Find("FeedbackImage/TextNotDone").GetComponent<TextMeshProFade>().FadeIn(this.textSlateFeedbackTransitionTime);
            yield return new WaitForSecondsRealtime(this.textSlateFeedbackWaitTime);
            textSlate.transform.Find("FeedbackImage").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/ArrowRight").GetComponent<ImageFade>().MakeInvisible();
            textSlate.transform.Find("FeedbackImage/TextNotDone").GetComponent<TextMeshProFade>().MakeInvisible();
        }
        WIDVTextSlateUtility.Instance.CallAttention();
    }

    public void CallAttention(bool _null = false)
    {
        foreach (GameObject textSlate in GameObject.FindGameObjectsWithTag("TextSlate"))
        {
            if (textSlate.activeSelf && !_null)
            {
                SwitchTracker.Instance.SetArrowTarget(textSlate);
            }
        }
        if (_null)
        {
            SwitchTracker.Instance.SetArrowTarget(null);
        }
    }
}
