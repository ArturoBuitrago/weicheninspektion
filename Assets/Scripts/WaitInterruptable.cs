﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitInterruptable : MonoBehaviour {

    private bool interrupt = false;

    public IEnumerator WaitCoroutine(float time)
    {
        this.interrupt = false;
        //Debug.Log("Inside coroutine");
        float t = 0.0f;

        while (t <= time && !this.interrupt)
        {
            //Debug.Log("We have waited for: " + t + " seconds");
            if (interrupt)
            {
                t = 0.0f;
                yield break;
            }
            t += Time.deltaTime;

            yield return null;
        }
        t = 0.0f;
        
    }

    public void Stop()
    {
        this.interrupt = true;        
    }
}
