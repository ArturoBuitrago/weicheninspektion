﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Viscopic.Holograms;

public class DBSceneSelector : MonoBehaviour {

    public ImageFade imgFade1;
    public ImageFade imgFade2;
    public ImageFade imgFade3;
    public FocusEnabler enabler1;
    public FocusEnabler enabler2;
    public FocusEnabler enabler3;
    public TextMeshProFade txtFade1;
    public TextMeshProFade txtFade2;
    public TextMeshProFade txtFade3;

    private List<ImageFade> imgFades = new List<ImageFade>();
    private List<FocusEnabler> enablers = new List<FocusEnabler>();
    private List<TextMeshProFade> txtFades = new List<TextMeshProFade>();

    private List<string> SceneNames = new List<string> {"S700K", "WeicheninspektionDV" ,""};

    private float fadeTime = 1.0f;
    

	// Use this for initialization
	void Start () {
        this.gameObject.transform.position = Camera.main.transform.position + Vector3.ProjectOnPlane(Camera.main.transform.forward, new Vector3(0, 1, 0)) * 2.0f;
        this.imgFades.Add(imgFade1);
        this.imgFades.Add(imgFade2);
        this.imgFades.Add(imgFade3);
        this.enablers.Add(enabler1);
        this.enablers.Add(enabler2);
        this.enablers.Add(enabler3);
        this.txtFades.Add(txtFade1);
        this.txtFades.Add(txtFade2);
        this.txtFades.Add(txtFade3);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SceneSelected(int sceneNumber)
    {
        if(sceneNumber > 3 || sceneNumber < 1)
        {
            throw new System.Exception("[DBSceneSelector] Scene number must be between 1 and 3");
        }
        int index = sceneNumber - 1;
        HashSet<int> scenes = new HashSet<int>{0, 1, 2};
        scenes.Remove(sceneNumber - 1);
        this.imgFades.RemoveAt(index);
        this.enablers.RemoveAt(index);
        this.txtFades.RemoveAt(index);
        Hide(scenes);
        StartCoroutine(Load(index));
    }

    private IEnumerator Load(int sceneNumber)
    {
        yield return new WaitForSeconds(this.fadeTime * 3);
        if(this.SceneNames[sceneNumber] == ""){
            throw new Exception("[DBSceneSelector] Chosen scene not ready yet.");
        }
        StartCoroutine(ViewLoader.Instance.LoadSceneAsync(this.SceneNames[sceneNumber]));
        //TransitionManager.Instance.LoadNextScene(this.SceneNames[sceneNumber], null);
        Destroy(this.gameObject);
    }

    private void Hide(HashSet<int> scenes)
    {
        foreach(int i in scenes)
        {
            foreach(ImageFade imgfade in this.imgFades)
            {
                imgfade.FadeOut(this.fadeTime);
            }
            foreach (FocusEnabler enabler in this.enablers)
            {
                enabler.enabled = false;
            }
            foreach (TextMeshProFade txtfade in this.txtFades)
            {
                txtfade.FadeOut(this.fadeTime);
            }

        }
    }




}
